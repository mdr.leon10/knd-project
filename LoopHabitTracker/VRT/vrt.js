var express = require('express')
var resemble = require('resemblejs')
var fs = require('file-system')
var app = express()
var shell = require('shelljs');

var num = Math.floor(Math.random() * 100) + 1;

app.get('/', (req) => {

    console.log('Starting regression tests')
    console.log('Taking new screenshots')
    shell.exec('rm -rf older');
    shell.exec('mkdir older');
    shell.exec('cp -r ./recent/ ./older/')
    shell.exec('rm -rf recent');
    shell.exec('mkdir recent');
    shell.exec('rm -rf ./screenshots')

    shell.exec('mkdir screenshots')
    shell.exec('cd screenshots')
    for(var i = 0; i < 100; i++){
        shell.exec('adb shell monkey -p org.isoron.uhabits -s '+ num +' -v 1');
        shell.exec('adb shell screencap /sdcard/pantallazo'+i+'.png');
        shell.exec('adb pull /sdcard/pantallazo'+i+'.png');
        shell.exec('adb shell rm /sdcard/pantallazo'+i+'.png');
        num = Math.floor(Math.random() * 100) + 1;
    }
    shell.exec('cd ..')

    shell.exec('cp -r ./screenshots ./recent')

    for(var k = 0; k < 100 ;k++){
        resemble('./recent/pantallazo'+k+'.png')
            .compareTo('./older/pantallazo'+k+'.png')
            .onComplete(function (data){
                console.log('Starting ResembleJS')
                fs.writeFile("./differences/pantallazo"+k+".png");
            })
    }
});
app.listen(3000, () => {
    console.log('Running regression tests')
})