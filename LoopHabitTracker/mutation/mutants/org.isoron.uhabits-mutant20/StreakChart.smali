.class public Lorg/isoron/uhabits/activities/common/views/StreakChart;
.super Landroid/view/View;
.source "StreakChart.java"


# instance fields
.field private baseSize:I

.field private colors:[I

.field private dateFormat:Ljava/text/DateFormat;

.field private em:F

.field private isBackgroundTransparent:Z

.field private maxLabelWidth:F

.field private maxLength:J

.field private minLength:J

.field private paint:Landroid/graphics/Paint;

.field private primaryColor:I

.field private rect:Landroid/graphics/RectF;

.field private reverseTextColor:I

.field private shouldShowLabels:Z

.field private streaks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/isoron/uhabits/models/Streak;",
            ">;"
        }
    .end annotation
.end field

.field private textColor:I

.field private textMargin:F

.field private width:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 76
    invoke-direct {p0}, Lorg/isoron/uhabits/activities/common/views/StreakChart;->init()V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 82
    invoke-direct {p0}, Lorg/isoron/uhabits/activities/common/views/StreakChart;->init()V

    .line 83
    return-void
.end method

.method private drawRow(Landroid/graphics/Canvas;Lorg/isoron/uhabits/models/Streak;Landroid/graphics/RectF;)V
    .locals 17
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "streak"    # Lorg/isoron/uhabits/models/Streak;
    .param p3, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 188
    move-object/from16 v0, p0

    iget-wide v2, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->maxLength:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lorg/isoron/uhabits/models/Streak;->getLength()J

    move-result-wide v2

    long-to-float v2, v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->maxLength:J

    long-to-float v3, v4

    div-float v14, v2, v3

    .line 191
    .local v14, "percentage":F
    move-object/from16 v0, p0

    iget v2, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->width:I

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v4, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->maxLabelWidth:F

    mul-float/2addr v3, v4

    sub-float v8, v2, v3

    .line 192
    .local v8, "availableWidth":F
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->shouldShowLabels:Z

    if-eqz v2, :cond_2

    const/high16 v2, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v3, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->textMargin:F

    mul-float/2addr v2, v3

    sub-float/2addr v8, v2

    .line 194
    :cond_2
    mul-float v9, v14, v8

    .line 195
    .local v9, "barWidth":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->paint:Landroid/graphics/Paint;

    .line 196
    invoke-virtual/range {p2 .. p2}, Lorg/isoron/uhabits/models/Streak;->getLength()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->em:F

    add-float v12, v2, v3

    .line 197
    .local v12, "minBarWidth":F
    invoke-static {v9, v12}, Ljava/lang/Math;->max(FF)F

    move-result v9

    .line 199
    move-object/from16 v0, p0

    iget v2, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->width:I

    int-to-float v2, v2

    sub-float/2addr v2, v9

    const/high16 v3, 0x40000000    # 2.0f

    div-float v11, v2, v3

    .line 200
    .local v11, "gap":F
    move-object/from16 v0, p0

    iget v2, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->baseSize:I

    int-to-float v2, v2

    const v3, 0x3d4ccccd    # 0.05f

    mul-float v13, v2, v3

    .line 202
    .local v13, "paddingTopBottom":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->paint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lorg/isoron/uhabits/activities/common/views/StreakChart;->percentageToColor(F)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 204
    move-object/from16 v0, p3

    iget v2, v0, Landroid/graphics/RectF;->left:F

    add-float v3, v2, v11

    move-object/from16 v0, p3

    iget v2, v0, Landroid/graphics/RectF;->top:F

    add-float v4, v2, v13

    move-object/from16 v0, p3

    iget v2, v0, Landroid/graphics/RectF;->right:F

    sub-float v5, v2, v11

    move-object/from16 v0, p3

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    sub-float v6, v2, v13

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->paint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 207
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    const v3, 0x3e99999a    # 0.3f

    move-object/from16 v0, p0

    iget v4, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->em:F

    mul-float/2addr v3, v4

    add-float v16, v2, v3

    .line 209
    .local v16, "yOffset":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->paint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v3, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->reverseTextColor:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 210
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->paint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 211
    invoke-virtual/range {p2 .. p2}, Lorg/isoron/uhabits/models/Streak;->getLength()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->paint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 214
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->shouldShowLabels:Z

    if-eqz v2, :cond_0

    .line 216
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->dateFormat:Ljava/text/DateFormat;

    new-instance v3, Ljava/util/Date;

    invoke-virtual/range {p2 .. p2}, Lorg/isoron/uhabits/models/Streak;->getStart()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v15

    .line 217
    .local v15, "startLabel":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->dateFormat:Ljava/text/DateFormat;

    new-instance v3, Ljava/util/Date;

    invoke-virtual/range {p2 .. p2}, Lorg/isoron/uhabits/models/Streak;->getEnd()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    .line 219
    .local v10, "endLabel":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->paint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v3, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->textColor:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 220
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->paint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 221
    move-object/from16 v0, p0

    iget v2, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->textMargin:F

    sub-float v2, v11, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->paint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v15, v2, v1, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 223
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->paint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 224
    move-object/from16 v0, p0

    iget v2, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->width:I

    int-to-float v2, v2

    sub-float/2addr v2, v11

    move-object/from16 v0, p0

    iget v3, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->textMargin:F

    add-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->paint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v10, v2, v1, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 230
    invoke-direct {p0}, Lorg/isoron/uhabits/activities/common/views/StreakChart;->initPaints()V

    .line 231
    invoke-direct {p0}, Lorg/isoron/uhabits/activities/common/views/StreakChart;->initColors()V

    .line 233
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->streaks:Ljava/util/List;

    .line 235
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->dateFormat:Ljava/text/DateFormat;

    .line 236
    iget-object v0, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->dateFormat:Ljava/text/DateFormat;

    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 237
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->rect:Landroid/graphics/RectF;

    .line 238
    invoke-virtual {p0}, Lorg/isoron/uhabits/activities/common/views/StreakChart;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090059

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->baseSize:I

    .line 239
    return-void
.end method

.method private initColors()V
    .locals 7

    .prologue
    .line 243
    iget v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->primaryColor:I

    invoke-static {v4}, Landroid/graphics/Color;->red(I)I

    move-result v2

    .line 244
    .local v2, "red":I
    iget v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->primaryColor:I

    invoke-static {v4}, Landroid/graphics/Color;->green(I)I

    move-result v1

    .line 245
    .local v1, "green":I
    iget v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->primaryColor:I

    invoke-static {v4}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    .line 247
    .local v0, "blue":I
    new-instance v3, Lorg/isoron/uhabits/utils/StyledResources;

    invoke-virtual {p0}, Lorg/isoron/uhabits/activities/common/views/StreakChart;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/isoron/uhabits/utils/StyledResources;-><init>(Landroid/content/Context;)V

    .line 249
    .local v3, "res":Lorg/isoron/uhabits/utils/StyledResources;
    const/4 v4, 0x4

    new-array v4, v4, [I

    iput-object v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->colors:[I

    .line 250
    iget-object v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->colors:[I

    const/4 v5, 0x3

    iget v6, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->primaryColor:I

    aput v6, v4, v5

    .line 251
    iget-object v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->colors:[I

    const/4 v5, 0x2

    const/16 v6, 0xc0

    invoke-static {v6, v2, v1, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v6

    aput v6, v4, v5

    .line 252
    iget-object v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->colors:[I

    const/4 v5, 0x1

    const/16 v6, 0x60

    invoke-static {v6, v2, v1, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v6

    aput v6, v4, v5

    .line 253
    iget-object v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->colors:[I

    const/4 v5, 0x0

    const v6, 0x7f010017

    invoke-virtual {v3, v6}, Lorg/isoron/uhabits/utils/StyledResources;->getColor(I)I

    move-result v6

    aput v6, v4, v5

    .line 254
    const v4, 0x7f010019

    invoke-virtual {v3, v4}, Lorg/isoron/uhabits/utils/StyledResources;->getColor(I)I

    move-result v4

    iput v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->textColor:I

    .line 255
    const v4, 0x7f010009

    invoke-virtual {v3, v4}, Lorg/isoron/uhabits/utils/StyledResources;->getColor(I)I

    move-result v4

    iput v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->reverseTextColor:I

    .line 256
    return-void
.end method

.method private initPaints()V
    .locals 2

    .prologue
    .line 260
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->paint:Landroid/graphics/Paint;

    .line 261
    iget-object v0, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 262
    iget-object v0, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->paint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 263
    return-void
.end method

.method private percentageToColor(F)I
    .locals 2
    .param p1, "percentage"    # F

    .prologue
    .line 267
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->colors:[I

    const/4 v1, 0x3

    aget v0, v0, v1

    .line 270
    :goto_0
    return v0

    .line 268
    :cond_0
    const v0, 0x3f4ccccd    # 0.8f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_1

    iget-object v0, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->colors:[I

    const/4 v1, 0x2

    aget v0, v0, v1

    goto :goto_0

    .line 269
    :cond_1
    const/high16 v0, 0x3f000000    # 0.5f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_2

    iget-object v0, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->colors:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    goto :goto_0

    .line 270
    :cond_2
    iget-object v0, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->colors:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    goto :goto_0
.end method

.method private updateMaxMinLengths()V
    .locals 10

    .prologue
    .line 275
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->maxLength:J

    .line 276
    const-wide v4, 0x7fffffffffffffffL

    iput-wide v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->minLength:J

    .line 277
    const/4 v3, 0x1

    iput-boolean v3, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->shouldShowLabels:Z

    .line 279
    iget-object v3, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->streaks:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/isoron/uhabits/models/Streak;

    .line 281
    .local v2, "s":Lorg/isoron/uhabits/models/Streak;
    iget-wide v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->maxLength:J

    invoke-virtual {v2}, Lorg/isoron/uhabits/models/Streak;->getLength()J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->maxLength:J

    .line 282
    iget-wide v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->minLength:J

    invoke-virtual {v2}, Lorg/isoron/uhabits/models/Streak;->getLength()J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->minLength:J

    .line 284
    iget-object v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->paint:Landroid/graphics/Paint;

    iget-object v5, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->dateFormat:Ljava/text/DateFormat;

    new-instance v6, Ljava/util/Date;

    .line 285
    invoke-virtual {v2}, Lorg/isoron/uhabits/models/Streak;->getStart()J

    move-result-wide v8

    invoke-direct {v6, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    .line 286
    .local v0, "lw1":F
    iget-object v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->paint:Landroid/graphics/Paint;

    iget-object v5, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->dateFormat:Ljava/text/DateFormat;

    new-instance v6, Ljava/util/Date;

    .line 287
    invoke-virtual {v2}, Lorg/isoron/uhabits/models/Streak;->getEnd()J

    move-result-wide v8

    const-wide v8, 0x255d2d0L

    invoke-direct {v6, v8, v9}, Ljava/util/Date;-><init>(J)V


    invoke-virtual {v5, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    .line 288
    .local v1, "lw2":F
    iget v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->maxLabelWidth:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    iput v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->maxLabelWidth:F

    goto :goto_0

    .line 291
    .end local v0    # "lw1":F
    .end local v1    # "lw2":F
    .end local v2    # "s":Lorg/isoron/uhabits/models/Streak;
    :cond_0
    iget v3, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->width:I

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    iget v5, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->maxLabelWidth:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->width:I

    int-to-float v4, v4

    const/high16 v5, 0x3e800000    # 0.25f

    mul-float/2addr v4, v5

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1

    .line 293
    const/4 v3, 0x0

    iput v3, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->maxLabelWidth:F

    .line 294
    const/4 v3, 0x0

    iput-boolean v3, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->shouldShowLabels:Z

    .line 296
    :cond_1
    return-void
.end method


# virtual methods
.method public getMaxStreakCount()I
    .locals 2

    .prologue
    .line 93
    invoke-virtual {p0}, Lorg/isoron/uhabits/activities/common/views/StreakChart;->getMeasuredHeight()I

    move-result v0

    iget v1, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->baseSize:I

    div-int/2addr v0, v1

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    .line 136
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 137
    iget-object v1, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->streaks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 146
    :cond_0
    return-void

    .line 139
    :cond_1
    iget-object v1, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->rect:Landroid/graphics/RectF;

    iget v2, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->width:I

    int-to-float v2, v2

    iget v3, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->baseSize:I

    int-to-float v3, v3

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 141
    iget-object v1, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->streaks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/isoron/uhabits/models/Streak;

    .line 143
    .local v0, "s":Lorg/isoron/uhabits/models/Streak;
    iget-object v2, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->rect:Landroid/graphics/RectF;

    invoke-direct {p0, p1, v0, v2}, Lorg/isoron/uhabits/activities/common/views/StreakChart;->drawRow(Landroid/graphics/Canvas;Lorg/isoron/uhabits/models/Streak;Landroid/graphics/RectF;)V

    .line 144
    iget-object v2, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->rect:Landroid/graphics/RectF;

    iget v3, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->baseSize:I

    int-to-float v3, v3

    invoke-virtual {v2, v4, v3}, Landroid/graphics/RectF;->offset(FF)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthSpec"    # I
    .param p2, "heightSpec"    # I

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 151
    invoke-virtual {p0}, Lorg/isoron/uhabits/activities/common/views/StreakChart;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 153
    .local v1, "params":Landroid/view/ViewGroup$LayoutParams;
    if-eqz v1, :cond_0

    iget v3, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v4, -0x2

    if-ne v3, v4, :cond_0

    .line 155
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 156
    .local v2, "width":I
    iget-object v3, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->streaks:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    iget v4, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->baseSize:I

    mul-int v0, v3, v4

    .line 158
    .local v0, "height":I
    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 159
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 162
    .end local v0    # "height":I
    .end local v2    # "width":I
    :cond_0
    invoke-virtual {p0, p1, p2}, Lorg/isoron/uhabits/activities/common/views/StreakChart;->setMeasuredDimension(II)V

    .line 163
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 6
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "oldWidth"    # I
    .param p4, "oldHeight"    # I

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    .line 171
    iput p1, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->width:I

    .line 173
    invoke-virtual {p0}, Lorg/isoron/uhabits/activities/common/views/StreakChart;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900c2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 175
    .local v1, "minTextSize":F
    invoke-virtual {p0}, Lorg/isoron/uhabits/activities/common/views/StreakChart;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900b9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 176
    .local v0, "maxTextSize":F
    iget v3, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->baseSize:I

    int-to-float v3, v3

    mul-float v2, v3, v5

    .line 178
    .local v2, "textSize":F
    iget-object v3, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->paint:Landroid/graphics/Paint;

    .line 179
    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v4

    invoke-static {v4, v1}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 178
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 180
    iget-object v3, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->paint:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getFontSpacing()F

    move-result v3

    iput v3, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->em:F

    .line 181
    iget v3, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->em:F

    mul-float/2addr v3, v5

    iput v3, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->textMargin:F

    .line 183
    invoke-direct {p0}, Lorg/isoron/uhabits/activities/common/views/StreakChart;->updateMaxMinLengths()V

    .line 184
    return-void
.end method

.method public populateWithRandomData()V
    .locals 12

    .prologue
    .line 98
    sget-wide v0, Lorg/isoron/uhabits/utils/DateUtils;->millisecondsInOneDay:J

    .line 99
    .local v0, "day":J
    invoke-static {}, Lorg/isoron/uhabits/utils/DateUtils;->getStartOfToday()J

    move-result-wide v6

    .line 100
    .local v6, "start":J
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    .line 102
    .local v8, "streaks":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lorg/isoron/uhabits/models/Streak;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    const/16 v9, 0xa

    if-ge v4, v9, :cond_0

    .line 104
    new-instance v9, Ljava/util/Random;

    invoke-direct {v9}, Ljava/util/Random;-><init>()V

    const/16 v10, 0x64

    invoke-virtual {v9, v10}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    .line 105
    .local v5, "length":I
    int-to-long v10, v5

    mul-long/2addr v10, v0

    add-long v2, v6, v10

    .line 106
    .local v2, "end":J
    new-instance v9, Lorg/isoron/uhabits/models/Streak;

    invoke-direct {v9, v6, v7, v2, v3}, Lorg/isoron/uhabits/models/Streak;-><init>(JJ)V

    invoke-virtual {v8, v9}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 107
    add-long v6, v2, v0

    .line 102
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 110
    .end local v2    # "end":J
    .end local v5    # "length":I
    :cond_0
    invoke-virtual {p0, v8}, Lorg/isoron/uhabits/activities/common/views/StreakChart;->setStreaks(Ljava/util/List;)V

    .line 111
    return-void
.end method

.method public setColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 115
    iput p1, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->primaryColor:I

    .line 116
    invoke-virtual {p0}, Lorg/isoron/uhabits/activities/common/views/StreakChart;->postInvalidate()V

    .line 117
    return-void
.end method

.method public setIsBackgroundTransparent(Z)V
    .locals 0
    .param p1, "isBackgroundTransparent"    # Z

    .prologue
    .line 121
    iput-boolean p1, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->isBackgroundTransparent:Z

    .line 122
    invoke-direct {p0}, Lorg/isoron/uhabits/activities/common/views/StreakChart;->initColors()V

    .line 123
    return-void
.end method

.method public setStreaks(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/isoron/uhabits/models/Streak;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 127
    .local p1, "streaks":Ljava/util/List;, "Ljava/util/List<Lorg/isoron/uhabits/models/Streak;>;"
    iput-object p1, p0, Lorg/isoron/uhabits/activities/common/views/StreakChart;->streaks:Ljava/util/List;

    .line 128
    invoke-direct {p0}, Lorg/isoron/uhabits/activities/common/views/StreakChart;->initColors()V

    .line 129
    invoke-direct {p0}, Lorg/isoron/uhabits/activities/common/views/StreakChart;->updateMaxMinLengths()V

    .line 130
    invoke-virtual {p0}, Lorg/isoron/uhabits/activities/common/views/StreakChart;->requestLayout()V

    .line 131
    return-void
.end method
