.class public Lorg/isoron/uhabits/widgets/views/GraphWidgetView;
.super Lorg/isoron/uhabits/widgets/views/HabitWidgetView;
.source "GraphWidgetView.java"


# instance fields
.field private final dataView:Landroid/view/View;

.field private title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dataView"    # Landroid/view/View;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lorg/isoron/uhabits/widgets/views/HabitWidgetView;-><init>(Landroid/content/Context;)V

    .line 39
    iput-object p2, p0, Lorg/isoron/uhabits/widgets/views/GraphWidgetView;->dataView:Landroid/view/View;

    .line 40
    invoke-direct {p0}, Lorg/isoron/uhabits/widgets/views/GraphWidgetView;->init()V

    .line 41
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 62
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 65
    .local v1, "params":Landroid/view/ViewGroup$LayoutParams;
    iget-object v2, p0, Lorg/isoron/uhabits/widgets/views/GraphWidgetView;->dataView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 67
    const v2, 0x7f0f00d7


    const/4 v0, 0x0


    .line 68
    .local v0, "innerFrame":Landroid/view/ViewGroup;
    iget-object v2, p0, Lorg/isoron/uhabits/widgets/views/GraphWidgetView;->dataView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 70
    const v2, 0x7f0f0016

    invoke-virtual {p0, v2}, Lorg/isoron/uhabits/widgets/views/GraphWidgetView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lorg/isoron/uhabits/widgets/views/GraphWidgetView;->title:Landroid/widget/TextView;

    .line 71
    iget-object v2, p0, Lorg/isoron/uhabits/widgets/views/GraphWidgetView;->title:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 72
    return-void
.end method


# virtual methods
.method public getDataView()Landroid/view/View;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lorg/isoron/uhabits/widgets/views/GraphWidgetView;->dataView:Landroid/view/View;

    return-object v0
.end method

.method protected getInnerLayoutId()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 57
    const v0, 0x7f040072

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 50
    iget-object v0, p0, Lorg/isoron/uhabits/widgets/views/GraphWidgetView;->title:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    return-void
.end method
