.class public abstract Lorg/isoron/uhabits/activities/BaseActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "BaseActivity.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# instance fields
.field private androidExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private baseMenu:Lorg/isoron/uhabits/activities/BaseMenu;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private component:Lorg/isoron/uhabits/activities/ActivityComponent;

.field private screen:Lorg/isoron/uhabits/activities/BaseScreen;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method

.method static synthetic access$lambda$0(Lorg/isoron/uhabits/activities/BaseActivity;)V
    .locals 0

    invoke-direct {p0}, Lorg/isoron/uhabits/activities/BaseActivity;->lambda$restartWithFade$0()V

    return-void
.end method

.method private synthetic lambda$restartWithFade$0()V
    .locals 3

    .prologue
    .line 86
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lorg/isoron/uhabits/automation/EditSettingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 87
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lorg/isoron/uhabits/activities/BaseActivity;->finish()V

    .line 88
    const/high16 v1, 0x10a0000

    const v2, 0x10a0001

    invoke-virtual {p0, v1, v2}, Lorg/isoron/uhabits/activities/BaseActivity;->overridePendingTransition(II)V

    .line 89
    invoke-virtual {p0, v0}, Lorg/isoron/uhabits/activities/BaseActivity;->startActivity(Landroid/content/Intent;)V

    .line 91
    return-void
.end method


# virtual methods
.method public getComponent()Lorg/isoron/uhabits/activities/ActivityComponent;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lorg/isoron/uhabits/activities/BaseActivity;->component:Lorg/isoron/uhabits/activities/ActivityComponent;

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "request"    # I
    .param p2, "result"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 146
    iget-object v0, p0, Lorg/isoron/uhabits/activities/BaseActivity;->screen:Lorg/isoron/uhabits/activities/BaseScreen;

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/AppCompatActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 148
    :goto_0
    return-void

    .line 147
    :cond_0
    iget-object v0, p0, Lorg/isoron/uhabits/activities/BaseActivity;->screen:Lorg/isoron/uhabits/activities/BaseScreen;

    invoke-virtual {v0, p1, p2, p3}, Lorg/isoron/uhabits/activities/BaseScreen;->onResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 153
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 155
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v1

    iput-object v1, p0, Lorg/isoron/uhabits/activities/BaseActivity;->androidExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 156
    invoke-static {p0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 158
    invoke-virtual {p0}, Lorg/isoron/uhabits/activities/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lorg/isoron/uhabits/HabitsApplication;

    .line 161
    .local v0, "app":Lorg/isoron/uhabits/HabitsApplication;
    invoke-static {}, Lorg/isoron/uhabits/activities/DaggerActivityComponent;->builder()Lorg/isoron/uhabits/activities/DaggerActivityComponent$Builder;

    move-result-object v1

    new-instance v2, Lorg/isoron/uhabits/activities/ActivityModule;

    invoke-direct {v2, p0}, Lorg/isoron/uhabits/activities/ActivityModule;-><init>(Lorg/isoron/uhabits/activities/BaseActivity;)V

    .line 162
    invoke-virtual {v1, v2}, Lorg/isoron/uhabits/activities/DaggerActivityComponent$Builder;->activityModule(Lorg/isoron/uhabits/activities/ActivityModule;)Lorg/isoron/uhabits/activities/DaggerActivityComponent$Builder;

    move-result-object v1

    .line 163
    invoke-virtual {v0}, Lorg/isoron/uhabits/HabitsApplication;->getComponent()Lorg/isoron/uhabits/AppComponent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/isoron/uhabits/activities/DaggerActivityComponent$Builder;->appComponent(Lorg/isoron/uhabits/AppComponent;)Lorg/isoron/uhabits/activities/DaggerActivityComponent$Builder;

    move-result-object v1

    .line 164
    invoke-virtual {v1}, Lorg/isoron/uhabits/activities/DaggerActivityComponent$Builder;->build()Lorg/isoron/uhabits/activities/ActivityComponent;

    move-result-object v1

    iput-object v1, p0, Lorg/isoron/uhabits/activities/BaseActivity;->component:Lorg/isoron/uhabits/activities/ActivityComponent;

    .line 166
    iget-object v1, p0, Lorg/isoron/uhabits/activities/BaseActivity;->component:Lorg/isoron/uhabits/activities/ActivityComponent;

    invoke-interface {v1}, Lorg/isoron/uhabits/activities/ActivityComponent;->getThemeSwitcher()Lorg/isoron/uhabits/activities/ThemeSwitcher;

    move-result-object v1

    invoke-virtual {v1}, Lorg/isoron/uhabits/activities/ThemeSwitcher;->apply()V

    .line 167
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 69
    if-nez p1, :cond_1

    .line 72
    :cond_0
    :goto_0
    return v2

    .line 70
    :cond_1
    iget-object v0, p0, Lorg/isoron/uhabits/activities/BaseActivity;->baseMenu:Lorg/isoron/uhabits/activities/BaseMenu;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lorg/isoron/uhabits/activities/BaseActivity;->baseMenu:Lorg/isoron/uhabits/activities/BaseMenu;

    invoke-virtual {p0}, Lorg/isoron/uhabits/activities/BaseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lorg/isoron/uhabits/activities/BaseMenu;->onCreate(Landroid/view/MenuInflater;Landroid/view/Menu;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 78
    if-nez p1, :cond_1

    .line 80
    :cond_0
    :goto_0
    return v0

    .line 79
    :cond_1
    iget-object v1, p0, Lorg/isoron/uhabits/activities/BaseActivity;->baseMenu:Lorg/isoron/uhabits/activities/BaseMenu;

    if-eqz v1, :cond_0

    .line 80
    iget-object v0, p0, Lorg/isoron/uhabits/activities/BaseActivity;->baseMenu:Lorg/isoron/uhabits/activities/BaseMenu;

    invoke-virtual {v0, p1}, Lorg/isoron/uhabits/activities/BaseMenu;->onItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public restartWithFade()V
    .locals 4

    .prologue
    .line 85
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-static {p0}, Lorg/isoron/uhabits/activities/BaseActivity$$Lambda$1;->lambdaFactory$(Lorg/isoron/uhabits/activities/BaseActivity;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 92
    return-void
.end method

.method public setBaseMenu(Lorg/isoron/uhabits/activities/BaseMenu;)V
    .locals 0
    .param p1, "baseMenu"    # Lorg/isoron/uhabits/activities/BaseMenu;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 96
    iput-object p1, p0, Lorg/isoron/uhabits/activities/BaseActivity;->baseMenu:Lorg/isoron/uhabits/activities/BaseMenu;

    .line 97
    return-void
.end method

.method public setScreen(Lorg/isoron/uhabits/activities/BaseScreen;)V
    .locals 0
    .param p1, "screen"    # Lorg/isoron/uhabits/activities/BaseScreen;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 101
    iput-object p1, p0, Lorg/isoron/uhabits/activities/BaseActivity;->screen:Lorg/isoron/uhabits/activities/BaseScreen;

    .line 102
    return-void
.end method

.method public showDialog(Landroid/support/v7/app/AppCompatDialog;)V
    .locals 0
    .param p1, "dialog"    # Landroid/support/v7/app/AppCompatDialog;

    .prologue
    .line 111
    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatDialog;->show()V

    .line 112
    return-void
.end method

.method public showDialog(Landroid/support/v7/app/AppCompatDialogFragment;Ljava/lang/String;)V
    .locals 1
    .param p1, "dialog"    # Landroid/support/v7/app/AppCompatDialogFragment;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 106
    invoke-virtual {p0}, Lorg/isoron/uhabits/activities/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/support/v7/app/AppCompatDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 107
    return-void
.end method

.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "thread"    # Ljava/lang/Thread;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "ex"    # Ljava/lang/Throwable;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 118
    if-nez p2, :cond_0

    .line 141
    :goto_0
    return-void

    .line 122
    :cond_0
    :try_start_0
    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 123
    new-instance v2, Lorg/isoron/uhabits/activities/BaseSystem;

    invoke-direct {v2, p0}, Lorg/isoron/uhabits/activities/BaseSystem;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lorg/isoron/uhabits/activities/BaseSystem;->dumpBugReportToFile()Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    :goto_1
    invoke-virtual {p2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    instance-of v2, v2, Lorg/isoron/uhabits/models/sqlite/InconsistentDatabaseException;

    if-eqz v2, :cond_1

    .line 132
    invoke-virtual {p0}, Lorg/isoron/uhabits/activities/BaseActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lorg/isoron/uhabits/HabitsApplication;

    .line 133
    .local v0, "app":Lorg/isoron/uhabits/HabitsApplication;
    invoke-virtual {v0}, Lorg/isoron/uhabits/HabitsApplication;->getComponent()Lorg/isoron/uhabits/AppComponent;

    move-result-object v2

    invoke-interface {v2}, Lorg/isoron/uhabits/AppComponent;->getHabitList()Lorg/isoron/uhabits/models/HabitList;

    move-result-object v1

    .line 134
    .local v1, "habits":Lorg/isoron/uhabits/models/HabitList;
    invoke-virtual {v1}, Lorg/isoron/uhabits/models/HabitList;->repair()V

    .line 135
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/System;->exit(I)V

    .line 138
    .end local v0    # "app":Lorg/isoron/uhabits/HabitsApplication;
    .end local v1    # "habits":Lorg/isoron/uhabits/models/HabitList;
    :cond_1
    iget-object v2, p0, Lorg/isoron/uhabits/activities/BaseActivity;->androidExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-eqz v2, :cond_2

    .line 139
    iget-object v2, p0, Lorg/isoron/uhabits/activities/BaseActivity;->androidExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v2, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 140
    :cond_2
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/System;->exit(I)V

    goto :goto_0

    .line 125
    :catch_0
    move-exception v2

    goto :goto_1
.end method
