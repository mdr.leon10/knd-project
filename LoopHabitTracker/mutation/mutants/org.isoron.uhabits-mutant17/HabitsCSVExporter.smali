.class public Lorg/isoron/uhabits/io/HabitsCSVExporter;
.super Ljava/lang/Object;
.source "HabitsCSVExporter.java"


# instance fields
.field private final DELIMITER:Ljava/lang/String;

.field private final allHabits:Lorg/isoron/uhabits/models/HabitList;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private exportDirName:Ljava/lang/String;

.field private generateDirs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private generateFilenames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private selectedHabits:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/isoron/uhabits/models/Habit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/isoron/uhabits/models/HabitList;Ljava/util/List;Ljava/io/File;)V
    .locals 2
    .param p1, "allHabits"    # Lorg/isoron/uhabits/models/HabitList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "dir"    # Ljava/io/File;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/isoron/uhabits/models/HabitList;",
            "Ljava/util/List",
            "<",
            "Lorg/isoron/uhabits/models/Habit;",
            ">;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    .prologue
    .line 55
    .local p2, "selectedHabits":Ljava/util/List;, "Ljava/util/List<Lorg/isoron/uhabits/models/Habit;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const-string v0, ","

    iput-object v0, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->DELIMITER:Ljava/lang/String;

    .line 56
    iput-object p1, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->allHabits:Lorg/isoron/uhabits/models/HabitList;

    .line 57
    iput-object p2, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->selectedHabits:Ljava/util/List;

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->exportDirName:Ljava/lang/String;

    .line 60
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->generateDirs:Ljava/util/List;

    .line 61
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->generateFilenames:Ljava/util/List;

    .line 62
    return-void
.end method

.method private addFileToZip(Ljava/util/zip/ZipOutputStream;Ljava/lang/String;)V
    .locals 7
    .param p1, "zos"    # Ljava/util/zip/ZipOutputStream;
    .param p2, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    new-instance v1, Ljava/io/FileInputStream;

    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->exportDirName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 80
    .local v1, "fis":Ljava/io/FileInputStream;
    new-instance v3, Ljava/util/zip/ZipEntry;

    invoke-direct {v3, p2}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V

    .line 81
    .local v3, "ze":Ljava/util/zip/ZipEntry;
    invoke-virtual {p1, v3}, Ljava/util/zip/ZipOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V

    .line 84
    const/16 v4, 0x400

    new-array v0, v4, [B

    .line 85
    .local v0, "bytes":[B
    :goto_0
    invoke-virtual {v1, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v2

    .local v2, "length":I
    if-ltz v2, :cond_0

    const/4 v4, 0x0

    invoke-virtual {p1, v0, v4, v2}, Ljava/util/zip/ZipOutputStream;->write([BII)V

    goto :goto_0

    .line 87
    :cond_0
    invoke-virtual {p1}, Ljava/util/zip/ZipOutputStream;->closeEntry()V

    .line 88
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 89
    return-void
.end method

.method private cleanup()V
    .locals 5

    .prologue
    .line 93
    iget-object v1, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->generateFilenames:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 94
    .local v0, "filename":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->exportDirName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 96
    .end local v0    # "filename":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->generateDirs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 97
    .restart local v0    # "filename":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->exportDirName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 99
    .end local v0    # "filename":Ljava/lang/String;
    :cond_1
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->exportDirName:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 100
    return-void
.end method

.method private getTimeframe()[J
    .locals 11

    .prologue
    .line 243
    const-wide v8, 0x7fffffffffffffffL

    .line 244
    .local v8, "oldest":J
    const-wide/16 v6, -0x1

    .line 245
    .local v6, "newest":J
    iget-object v5, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->selectedHabits:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/isoron/uhabits/models/Habit;

    .line 247
    .local v4, "h":Lorg/isoron/uhabits/models/Habit;
    invoke-virtual {v4}, Lorg/isoron/uhabits/models/Habit;->getRepetitions()Lorg/isoron/uhabits/models/RepetitionList;

    move-result-object v10

    invoke-virtual {v10}, Lorg/isoron/uhabits/models/RepetitionList;->getOldest()Lorg/isoron/uhabits/models/Repetition;

    move-result-object v10

    if-eqz v10, :cond_0

    invoke-virtual {v4}, Lorg/isoron/uhabits/models/Habit;->getRepetitions()Lorg/isoron/uhabits/models/RepetitionList;

    move-result-object v10

    invoke-virtual {v10}, Lorg/isoron/uhabits/models/RepetitionList;->getNewest()Lorg/isoron/uhabits/models/Repetition;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 249
    invoke-virtual {v4}, Lorg/isoron/uhabits/models/Habit;->getRepetitions()Lorg/isoron/uhabits/models/RepetitionList;

    move-result-object v10

    invoke-virtual {v10}, Lorg/isoron/uhabits/models/RepetitionList;->getOldest()Lorg/isoron/uhabits/models/Repetition;

    move-result-object v10

    invoke-virtual {v10}, Lorg/isoron/uhabits/models/Repetition;->getTimestamp()J

    move-result-wide v2

    .line 250
    .local v2, "currOld":J
    invoke-virtual {v4}, Lorg/isoron/uhabits/models/Habit;->getRepetitions()Lorg/isoron/uhabits/models/RepetitionList;

    move-result-object v10

    invoke-virtual {v10}, Lorg/isoron/uhabits/models/RepetitionList;->getNewest()Lorg/isoron/uhabits/models/Repetition;

    move-result-object v10

    invoke-virtual {v10}, Lorg/isoron/uhabits/models/Repetition;->getTimestamp()J

    move-result-wide v0

    .line 251
    .local v0, "currNew":J
    cmp-long v10, v2, v8

    if-lez v10, :cond_1

    .line 252
    :goto_1
    cmp-long v10, v0, v6

    if-gez v10, :cond_2

    .line 253
    :goto_2
    goto :goto_0

    :cond_1
    move-wide v8, v2

    .line 251
    goto :goto_1

    :cond_2
    move-wide v6, v0

    .line 252
    goto :goto_2

    .line 254
    .end local v0    # "currNew":J
    .end local v2    # "currOld":J
    .end local v4    # "h":Lorg/isoron/uhabits/models/Habit;
    :cond_3
    const/4 v5, 0x2

    new-array v5, v5, [J

    const/4 v10, 0x0

    aput-wide v8, v5, v10

    const/4 v10, 0x1

    aput-wide v6, v5, v10

    return-object v5
.end method

.method private sanitizeFilename(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 105
    const-string v1, "[^ a-zA-Z0-9\\._-]+"

    const-string v2, ""

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 106
    .local v0, "s":Ljava/lang/String;
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x64

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private writeCheckmarks(Ljava/lang/String;Lorg/isoron/uhabits/models/CheckmarkList;)V
    .locals 4
    .param p1, "habitDirName"    # Ljava/lang/String;
    .param p2, "checkmarks"    # Lorg/isoron/uhabits/models/CheckmarkList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Checkmarks.csv"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 149
    .local v0, "filename":Ljava/lang/String;
    new-instance v1, Ljava/io/FileWriter;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->exportDirName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    .line 150
    .local v1, "out":Ljava/io/FileWriter;
    iget-object v2, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->generateFilenames:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    invoke-virtual {p2, v1}, Lorg/isoron/uhabits/models/CheckmarkList;->writeCSV(Ljava/io/Writer;)V

    .line 152
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V

    .line 153
    return-void
.end method

.method private writeHabits()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    const-string v0, "Habits.csv"

    .line 112
    .local v0, "filename":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->exportDirName:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    .line 113
    new-instance v3, Ljava/io/FileWriter;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->exportDirName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    .line 114
    .local v3, "out":Ljava/io/FileWriter;
    iget-object v5, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->generateFilenames:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    iget-object v5, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->allHabits:Lorg/isoron/uhabits/models/HabitList;

    invoke-virtual {v5, v3}, Lorg/isoron/uhabits/models/HabitList;->writeCSV(Ljava/io/Writer;)V

    .line 116
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V

    .line 118
    iget-object v5, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->selectedHabits:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/isoron/uhabits/models/Habit;

    .line 120
    .local v1, "h":Lorg/isoron/uhabits/models/Habit;
    invoke-virtual {v1}, Lorg/isoron/uhabits/models/Habit;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lorg/isoron/uhabits/io/HabitsCSVExporter;->sanitizeFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 121
    .local v4, "sane":Ljava/lang/String;
    const-string v6, "%03d %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->allHabits:Lorg/isoron/uhabits/models/HabitList;

    .line 122
    invoke-virtual {v9, v1}, Lorg/isoron/uhabits/models/HabitList;->indexOf(Lorg/isoron/uhabits/models/Habit;)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object v4, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 123
    .local v2, "habitDirName":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 125
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->exportDirName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->mkdirs()Z

    .line 126
    iget-object v6, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->generateDirs:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    invoke-virtual {v1}, Lorg/isoron/uhabits/models/Habit;->getScores()Lorg/isoron/uhabits/models/ScoreList;

    move-result-object v6

    invoke-direct {p0, v2, v6}, Lorg/isoron/uhabits/io/HabitsCSVExporter;->writeScores(Ljava/lang/String;Lorg/isoron/uhabits/models/ScoreList;)V

    .line 129
    invoke-virtual {v1}, Lorg/isoron/uhabits/models/Habit;->getCheckmarks()Lorg/isoron/uhabits/models/CheckmarkList;

    move-result-object v6

    invoke-direct {p0, v2, v6}, Lorg/isoron/uhabits/io/HabitsCSVExporter;->writeCheckmarks(Ljava/lang/String;Lorg/isoron/uhabits/models/CheckmarkList;)V

    goto :goto_0

    .line 132
    .end local v1    # "h":Lorg/isoron/uhabits/models/Habit;
    .end local v2    # "habitDirName":Ljava/lang/String;
    .end local v4    # "sane":Ljava/lang/String;
    :cond_0
    invoke-direct {p0}, Lorg/isoron/uhabits/io/HabitsCSVExporter;->writeMultipleHabits()V

    .line 133
    return-void
.end method

.method private writeMultipleHabits()V
    .locals 27
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    const-string v19, "Scores.csv"

    .line 167
    .local v19, "scoresFileName":Ljava/lang/String;
    const-string v3, "Checkmarks.csv"

    .line 168
    .local v3, "checksFileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->generateFilenames:Ljava/util/List;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->generateFilenames:Ljava/util/List;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    new-instance v20, Ljava/io/FileWriter;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->exportDirName:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    .line 171
    .local v20, "scoresWriter":Ljava/io/FileWriter;
    new-instance v4, Ljava/io/FileWriter;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->exportDirName:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v4, v0}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    .line 173
    .local v4, "checksWriter":Ljava/io/FileWriter;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/isoron/uhabits/io/HabitsCSVExporter;->writeMultipleHabitsHeader(Ljava/io/Writer;)V

    .line 174
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lorg/isoron/uhabits/io/HabitsCSVExporter;->writeMultipleHabitsHeader(Ljava/io/Writer;)V

    .line 176
    invoke-direct/range {p0 .. p0}, Lorg/isoron/uhabits/io/HabitsCSVExporter;->getTimeframe()[J

    move-result-object v21

    .line 177
    .local v21, "timeframe":[J
    const/16 v22, 0x0

    aget-wide v14, v21, v22

    .line 178
    .local v14, "oldest":J
    invoke-static {}, Lorg/isoron/uhabits/utils/DateUtils;->getStartOfToday()J

    move-result-wide v12

    .line 180
    .local v12, "newest":J
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 181
    .local v2, "checkmarks":Ljava/util/List;, "Ljava/util/List<[I>;"
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 182
    .local v18, "scores":Ljava/util/List;, "Ljava/util/List<[I>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->selectedHabits:Ljava/util/List;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_0
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_0

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/isoron/uhabits/models/Habit;

    .line 184
    .local v9, "h":Lorg/isoron/uhabits/models/Habit;
    invoke-virtual {v9}, Lorg/isoron/uhabits/models/Habit;->getCheckmarks()Lorg/isoron/uhabits/models/CheckmarkList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v14, v15, v12, v13}, Lorg/isoron/uhabits/models/CheckmarkList;->getValues(JJ)[I

    move-result-object v23

    move-object/from16 v0, v23

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    invoke-virtual {v9}, Lorg/isoron/uhabits/models/Habit;->getScores()Lorg/isoron/uhabits/models/ScoreList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v14, v15, v12, v13}, Lorg/isoron/uhabits/models/ScoreList;->getValues(JJ)[I

    move-result-object v23

    move-object/from16 v0, v18

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 188
    .end local v9    # "h":Lorg/isoron/uhabits/models/Habit;
    :cond_0
    invoke-static {v14, v15, v12, v13}, Lorg/isoron/uhabits/utils/DateUtils;->getDaysBetween(JJ)I

    move-result v8

    .line 189
    .local v8, "days":I
    invoke-static {}, Lorg/isoron/uhabits/utils/DateFormats;->getCSVDateFormat()Ljava/text/SimpleDateFormat;

    move-result-object v6

    .line 190
    .local v6, "dateFormat":Ljava/text/SimpleDateFormat;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    if-gt v10, v8, :cond_2

    .line 192
    new-instance v7, Ljava/util/Date;

    int-to-long v0, v10

    move-wide/from16 v22, v0

    sget-wide v24, Lorg/isoron/uhabits/utils/DateUtils;->millisecondsInOneDay:J

    mul-long v22, v22, v24

    sub-long v22, v12, v22

    move-wide/from16 v0, v22

    invoke-direct {v7, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 194
    .local v7, "day":Ljava/util/Date;
    invoke-virtual {v6, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 195
    .local v5, "date":Ljava/lang/String;
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 196
    .local v16, "sb":Ljava/lang/StringBuilder;
    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ","

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 198
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 200
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->selectedHabits:Ljava/util/List;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v22

    move/from16 v0, v22

    if-ge v11, v0, :cond_1

    .line 202
    invoke-interface {v2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, [I

    aget v22, v22, v10

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 203
    const-string v22, ","

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 204
    const-string v23, "%.4f"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    .line 205
    move-object/from16 v0, v18

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, [I

    aget v22, v22, v10

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    const v26, 0x4b92f02b    # 1.9259478E7f

    div-float v22, v22, v26

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    aput-object v22, v24, v25

    invoke-static/range {v23 .. v24}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    .line 206
    .local v17, "score":Ljava/lang/String;
    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 207
    const-string v22, ","

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 200
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 209
    .end local v17    # "score":Ljava/lang/String;
    :cond_1
    const-string v22, "\n"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 210
    const-string v22, "\n"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 190
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 212
    .end local v5    # "date":Ljava/lang/String;
    .end local v7    # "day":Ljava/util/Date;
    .end local v11    # "j":I
    .end local v16    # "sb":Ljava/lang/StringBuilder;
    :cond_2
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileWriter;->close()V

    .line 213
    invoke-virtual {v4}, Ljava/io/FileWriter;->close()V

    .line 214
    return-void
.end method

.method private writeMultipleHabitsHeader(Ljava/io/Writer;)V
    .locals 3
    .param p1, "out"    # Ljava/io/Writer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 225
    const-string v1, "Date,"

    invoke-virtual {p1, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 226
    iget-object v1, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->selectedHabits:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/isoron/uhabits/models/Habit;

    .line 227
    .local v0, "h":Lorg/isoron/uhabits/models/Habit;
    invoke-virtual {v0}, Lorg/isoron/uhabits/models/Habit;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 228
    const-string v2, ","

    invoke-virtual {p1, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_0

    .line 230
    .end local v0    # "h":Lorg/isoron/uhabits/models/Habit;
    :cond_0
    const-string v1, "\n"

    invoke-virtual {p1, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 231
    return-void
.end method

.method private writeScores(Ljava/lang/String;Lorg/isoron/uhabits/models/ScoreList;)V
    .locals 4
    .param p1, "habitDirName"    # Ljava/lang/String;
    .param p2, "scores"    # Lorg/isoron/uhabits/models/ScoreList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Scores.csv"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 139
    .local v1, "path":Ljava/lang/String;
    new-instance v0, Ljava/io/FileWriter;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->exportDirName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    .line 140
    .local v0, "out":Ljava/io/FileWriter;
    iget-object v2, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->generateFilenames:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    invoke-virtual {p2, v0}, Lorg/isoron/uhabits/models/ScoreList;->writeCSV(Ljava/io/Writer;)V

    .line 142
    invoke-virtual {v0}, Ljava/io/FileWriter;->close()V

    .line 143
    return-void
.end method

.method private writeZipFile()Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 259
    invoke-static {}, Lorg/isoron/uhabits/utils/DateFormats;->getCSVDateFormat()Ljava/text/SimpleDateFormat;

    move-result-object v1

    .line 260
    .local v1, "dateFormat":Ljava/text/SimpleDateFormat;
    invoke-static {}, Lorg/isoron/uhabits/utils/DateUtils;->getStartOfToday()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 261
    .local v0, "date":Ljava/lang/String;
    const-string v6, "%s/Loop Habits CSV %s.zip"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->exportDirName:Ljava/lang/String;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object v0, v7, v8

    .line 262
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 264
    .local v4, "zipFilename":Ljava/lang/String;
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 265
    .local v3, "fos":Ljava/io/FileOutputStream;
    new-instance v5, Ljava/util/zip/ZipOutputStream;

    invoke-direct {v5, v3}, Ljava/util/zip/ZipOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 267
    .local v5, "zos":Ljava/util/zip/ZipOutputStream;
    iget-object v6, p0, Lorg/isoron/uhabits/io/HabitsCSVExporter;->generateFilenames:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 268
    .local v2, "filename":Ljava/lang/String;
    invoke-direct {p0, v5, v2}, Lorg/isoron/uhabits/io/HabitsCSVExporter;->addFileToZip(Ljava/util/zip/ZipOutputStream;Ljava/lang/String;)V

    goto :goto_0

    .line 270
    .end local v2    # "filename":Ljava/lang/String;
    :cond_0
    invoke-virtual {v5}, Ljava/util/zip/ZipOutputStream;->close()V

    .line 271
    const/4 v3, 0x0

    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 273
    return-object v4
.end method


# virtual methods
.method public writeArchive()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    invoke-direct {p0}, Lorg/isoron/uhabits/io/HabitsCSVExporter;->writeHabits()V

    .line 69
    invoke-direct {p0}, Lorg/isoron/uhabits/io/HabitsCSVExporter;->writeZipFile()Ljava/lang/String;

    move-result-object v0

    .line 70
    .local v0, "zipFilename":Ljava/lang/String;
    invoke-direct {p0}, Lorg/isoron/uhabits/io/HabitsCSVExporter;->cleanup()V

    .line 72
    return-object v0
.end method
