.class public Lorg/isoron/uhabits/activities/about/AboutActivity;
.super Lorg/isoron/uhabits/activities/BaseActivity;
.source "AboutActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/isoron/uhabits/activities/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method private delay()V
    .locals 2

    .line 332
    const-wide/16 v0, 0x2710

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 335
    goto :goto_0

    .line 333
    :catch_0
    move-exception v0

    .line 334
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 336
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :goto_0
    return-void
.end method
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 36
    invoke-super {p0, p1}, Lorg/isoron/uhabits/activities/BaseActivity;->onCreate(Landroid/os/Bundle;)V
    invoke-direct/range {p0 .. p0}, Lorg/isoron/uhabits/activities/about/AboutActivity;->delay()V

    .line 37
    new-instance v0, Lorg/isoron/uhabits/activities/about/AboutRootView;

    new-instance v2, Lorg/isoron/uhabits/intents/IntentFactory;

    invoke-direct {v2}, Lorg/isoron/uhabits/intents/IntentFactory;-><init>()V

    invoke-direct {v0, p0, v2}, Lorg/isoron/uhabits/activities/about/AboutRootView;-><init>(Landroid/content/Context;Lorg/isoron/uhabits/intents/IntentFactory;)V

    .line 38
    .local v0, "rootView":Lorg/isoron/uhabits/activities/about/AboutRootView;
    new-instance v1, Lorg/isoron/uhabits/activities/BaseScreen;

    invoke-direct {v1, p0}, Lorg/isoron/uhabits/activities/BaseScreen;-><init>(Lorg/isoron/uhabits/activities/BaseActivity;)V

    .line 39
    .local v1, "screen":Lorg/isoron/uhabits/activities/BaseScreen;
    invoke-virtual {v1, v0}, Lorg/isoron/uhabits/activities/BaseScreen;->setRootView(Lorg/isoron/uhabits/activities/BaseRootView;)V

    .line 40
    invoke-virtual {p0, v1}, Lorg/isoron/uhabits/activities/about/AboutActivity;->setScreen(Lorg/isoron/uhabits/activities/BaseScreen;)V

    .line 41
    return-void
.end method
