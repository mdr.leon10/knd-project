.class public abstract Lorg/isoron/uhabits/widgets/BaseWidget;
.super Ljava/lang/Object;
.source "BaseWidget.java"


# instance fields
.field private final context:Landroid/content/Context;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private dimensions:Lorg/isoron/uhabits/widgets/WidgetDimensions;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private final id:I

.field protected final pendingIntentFactory:Lorg/isoron/uhabits/intents/PendingIntentFactory;

.field private final prefs:Lorg/isoron/uhabits/preferences/WidgetPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "id"    # I

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput p2, p0, Lorg/isoron/uhabits/widgets/BaseWidget;->id:I

    .line 54
    iput-object p1, p0, Lorg/isoron/uhabits/widgets/BaseWidget;->context:Landroid/content/Context;

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lorg/isoron/uhabits/HabitsApplication;

    .line 59
    .local v0, "app":Lorg/isoron/uhabits/HabitsApplication;
    invoke-virtual {v0}, Lorg/isoron/uhabits/HabitsApplication;->getComponent()Lorg/isoron/uhabits/AppComponent;

    move-result-object v1

    invoke-interface {v1}, Lorg/isoron/uhabits/AppComponent;->getWidgetPreferences()Lorg/isoron/uhabits/preferences/WidgetPreferences;

    move-result-object v1

    iput-object v1, p0, Lorg/isoron/uhabits/widgets/BaseWidget;->prefs:Lorg/isoron/uhabits/preferences/WidgetPreferences;

    .line 60
    invoke-virtual {v0}, Lorg/isoron/uhabits/HabitsApplication;->getComponent()Lorg/isoron/uhabits/AppComponent;

    move-result-object v1

    invoke-interface {v1}, Lorg/isoron/uhabits/AppComponent;->getPendingIntentFactory()Lorg/isoron/uhabits/intents/PendingIntentFactory;

    move-result-object v1

    iput-object v1, p0, Lorg/isoron/uhabits/widgets/BaseWidget;->pendingIntentFactory:Lorg/isoron/uhabits/intents/PendingIntentFactory;

    .line 61
    new-instance v1, Lorg/isoron/uhabits/widgets/WidgetDimensions;

    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/BaseWidget;->getDefaultWidth()I

    move-result v2

    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/BaseWidget;->getDefaultHeight()I

    move-result v3

    .line 62
    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/BaseWidget;->getDefaultWidth()I

    move-result v4

    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/BaseWidget;->getDefaultHeight()I

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/isoron/uhabits/widgets/WidgetDimensions;-><init>(IIII)V

    iput-object v1, p0, Lorg/isoron/uhabits/widgets/BaseWidget;->dimensions:Lorg/isoron/uhabits/widgets/WidgetDimensions;

    .line 63
    return-void
.end method

.method private adjustRemoteViewsPadding(Landroid/widget/RemoteViews;Landroid/view/View;II)V
    .locals 9
    .param p1, "remoteViews"    # Landroid/widget/RemoteViews;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 115
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    .line 116
    .local v7, "imageWidth":I
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 117
    .local v6, "imageHeight":I
    invoke-direct {p0, p3, p4, v7, v6}, Lorg/isoron/uhabits/widgets/BaseWidget;->calculatePadding(IIII)[I

    move-result-object v8

    .line 118
    .local v8, "p":[I
    const v1, 0x7f0f011d

    const/4 v0, 0x0

    aget v2, v8, v0

    const/4 v0, 0x1

    aget v3, v8, v0

    const/4 v0, 0x2

    aget v4, v8, v0

    const/4 v0, 0x3

    aget v5, v8, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    .line 119
    return-void
.end method

.method private buildRemoteViews(Landroid/view/View;Landroid/widget/RemoteViews;II)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "remoteViews"    # Landroid/widget/RemoteViews;
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 126
    invoke-direct {p0, p1}, Lorg/isoron/uhabits/widgets/BaseWidget;->getBitmapFromView(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 127
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const v2, 0x7f0f011c

    invoke-virtual {p2, v2, v0}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 129
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_0

    .line 130
    invoke-direct {p0, p2, p1, p3, p4}, Lorg/isoron/uhabits/widgets/BaseWidget;->adjustRemoteViewsPadding(Landroid/widget/RemoteViews;Landroid/view/View;II)V

    .line 132
    :cond_0
    iget-object v2, p0, Lorg/isoron/uhabits/widgets/BaseWidget;->context:Landroid/content/Context;

    invoke-virtual {p0, v2}, Lorg/isoron/uhabits/widgets/BaseWidget;->getOnClickPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 133
    .local v1, "onClickIntent":Landroid/app/PendingIntent;
    if-eqz v1, :cond_1

    .line 134
    const v2, 0x7f0f011e

    invoke-virtual {p2, v2, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 135
    :cond_1
    return-void
.end method

.method private calculatePadding(IIII)[I
    .locals 5
    .param p1, "entireWidth"    # I
    .param p2, "entireHeight"    # I
    .param p3, "imageWidth"    # I
    .param p4, "imageHeight"    # I

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 142
    int-to-float v2, p1

    int-to-float v3, p3

    sub-float/2addr v2, v3

    div-float/2addr v2, v4

    float-to-int v1, v2

    .line 143
    .local v1, "w":I
    int-to-float v2, p2

    int-to-float v3, p4

    sub-float/2addr v2, v3

    div-float/2addr v2, v4

    float-to-int v0, v2

    .line 145
    .local v0, "h":I
    const/4 v2, 0x4

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput v1, v2, v3

    const/4 v3, 0x1

    aput v0, v2, v3

    const/4 v3, 0x2

    aput v1, v2, v3

    const/4 v3, 0x3

    aput v0, v2, v3

    return-object v2
.end method

.method private getBitmapFromView(Landroid/view/View;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 151
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 152
    invoke-virtual {p1, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 153
    invoke-virtual {p1, v1}, Landroid/view/View;->buildDrawingCache(Z)V

    .line 154
    invoke-virtual {p1}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 156
    .local v0, "drawingCache":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 157
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "bitmap is null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 159
    :cond_0
    return-object v0
.end method

.method private getRemoteViews(II)Landroid/widget/RemoteViews;
    .locals 4
    .param p1, "width"    # I
    .param p2, "height"    # I
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 165
    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/BaseWidget;->buildView()Landroid/view/View;

    move-result-object v1

    .line 166
    .local v1, "view":Landroid/view/View;
    invoke-direct {p0, v1, p1, p2}, Lorg/isoron/uhabits/widgets/BaseWidget;->measureView(Landroid/view/View;II)V

    .line 168
    invoke-virtual {p0, v1}, Lorg/isoron/uhabits/widgets/BaseWidget;->refreshData(Landroid/view/View;)V

    .line 170
    invoke-virtual {v1}, Landroid/view/View;->isLayoutRequested()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v1, p1, p2}, Lorg/isoron/uhabits/widgets/BaseWidget;->measureView(Landroid/view/View;II)V

    .line 172
    :cond_0
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v2, p0, Lorg/isoron/uhabits/widgets/BaseWidget;->context:Landroid/content/Context;

    .line 173
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f040073

    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 175
    .local v0, "remoteViews":Landroid/widget/RemoteViews;
    invoke-direct {p0, v1, v0, p1, p2}, Lorg/isoron/uhabits/widgets/BaseWidget;->buildRemoteViews(Landroid/view/View;Landroid/widget/RemoteViews;II)V

    .line 177
    return-object v0
.end method

.method private measureView(Landroid/view/View;II)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v7, 0x0

    .line 182
    iget-object v5, p0, Lorg/isoron/uhabits/widgets/BaseWidget;->context:Landroid/content/Context;

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 183
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f040073

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 185
    .local v0, "entireView":Landroid/view/View;
    invoke-static {p2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 186
    .local v4, "specWidth":I
    invoke-static {p3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 188
    .local v3, "specHeight":I
    invoke-virtual {v0, v4, v3}, Landroid/view/View;->measure(II)V

    .line 189
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 190
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 189
    invoke-virtual {v0, v7, v7, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 192
    const v5, 0x7f0f011c


    const/4 v1, 0x0


    .line 193
    .local v1, "imageView":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p2

    .line 194
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p3

    .line 196
    invoke-static {p2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 197
    invoke-static {p3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 199
    invoke-virtual {p1, v4, v3}, Landroid/view/View;->measure(II)V

    .line 200
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    invoke-virtual {p1, v7, v7, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 201
    return-void
.end method


# virtual methods
.method protected abstract buildView()Landroid/view/View;
.end method

.method public delete()V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lorg/isoron/uhabits/widgets/BaseWidget;->prefs:Lorg/isoron/uhabits/preferences/WidgetPreferences;

    iget v1, p0, Lorg/isoron/uhabits/widgets/BaseWidget;->id:I

    invoke-virtual {v0, v1}, Lorg/isoron/uhabits/preferences/WidgetPreferences;->removeWidget(I)V

    .line 68
    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lorg/isoron/uhabits/widgets/BaseWidget;->context:Landroid/content/Context;

    return-object v0
.end method

.method protected abstract getDefaultHeight()I
.end method

.method protected abstract getDefaultWidth()I
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lorg/isoron/uhabits/widgets/BaseWidget;->id:I

    return v0
.end method

.method public getLandscapeRemoteViews()Landroid/widget/RemoteViews;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lorg/isoron/uhabits/widgets/BaseWidget;->dimensions:Lorg/isoron/uhabits/widgets/WidgetDimensions;

    invoke-virtual {v0}, Lorg/isoron/uhabits/widgets/WidgetDimensions;->getLandscapeWidth()I

    move-result v0

    iget-object v1, p0, Lorg/isoron/uhabits/widgets/BaseWidget;->dimensions:Lorg/isoron/uhabits/widgets/WidgetDimensions;

    .line 85
    invoke-virtual {v1}, Lorg/isoron/uhabits/widgets/WidgetDimensions;->getLandscapeHeight()I

    move-result v1

    .line 84
    invoke-direct {p0, v0, v1}, Lorg/isoron/uhabits/widgets/BaseWidget;->getRemoteViews(II)Landroid/widget/RemoteViews;

    move-result-object v0

    return-object v0
.end method

.method public abstract getOnClickPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;
.end method

.method public getPortraitRemoteViews()Landroid/widget/RemoteViews;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lorg/isoron/uhabits/widgets/BaseWidget;->dimensions:Lorg/isoron/uhabits/widgets/WidgetDimensions;

    invoke-virtual {v0}, Lorg/isoron/uhabits/widgets/WidgetDimensions;->getPortraitWidth()I

    move-result v0

    iget-object v1, p0, Lorg/isoron/uhabits/widgets/BaseWidget;->dimensions:Lorg/isoron/uhabits/widgets/WidgetDimensions;

    .line 94
    invoke-virtual {v1}, Lorg/isoron/uhabits/widgets/WidgetDimensions;->getPortraitHeight()I

    move-result v1

    .line 93
    invoke-direct {p0, v0, v1}, Lorg/isoron/uhabits/widgets/BaseWidget;->getRemoteViews(II)Landroid/widget/RemoteViews;

    move-result-object v0

    return-object v0
.end method

.method public abstract refreshData(Landroid/view/View;)V
.end method

.method public setDimensions(Lorg/isoron/uhabits/widgets/WidgetDimensions;)V
    .locals 0
    .param p1, "dimensions"    # Lorg/isoron/uhabits/widgets/WidgetDimensions;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 101
    iput-object p1, p0, Lorg/isoron/uhabits/widgets/BaseWidget;->dimensions:Lorg/isoron/uhabits/widgets/WidgetDimensions;

    .line 102
    return-void
.end method
