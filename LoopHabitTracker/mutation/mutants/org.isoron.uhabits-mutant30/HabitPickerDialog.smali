.class public Lorg/isoron/uhabits/widgets/HabitPickerDialog;
.super Landroid/app/Activity;
.source "HabitPickerDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private habitIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private habitList:Lorg/isoron/uhabits/models/HabitList;

.field private preferences:Lorg/isoron/uhabits/preferences/WidgetPreferences;

.field private widgetId:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 68
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 69
    const v8, 0x7f040070

    invoke-virtual {p0, v8}, Lorg/isoron/uhabits/widgets/HabitPickerDialog;->setContentView(I)V

    .line 71
    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/HabitPickerDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lorg/isoron/uhabits/HabitsApplication;

    .line 72
    .local v1, "app":Lorg/isoron/uhabits/HabitsApplication;
    invoke-virtual {v1}, Lorg/isoron/uhabits/HabitsApplication;->getComponent()Lorg/isoron/uhabits/AppComponent;

    move-result-object v2

    .line 73
    .local v2, "component":Lorg/isoron/uhabits/AppComponent;
    invoke-interface {v2}, Lorg/isoron/uhabits/AppComponent;->getHabitList()Lorg/isoron/uhabits/models/HabitList;

    move-result-object v8

    iput-object v8, p0, Lorg/isoron/uhabits/widgets/HabitPickerDialog;->habitList:Lorg/isoron/uhabits/models/HabitList;

    .line 74
    invoke-interface {v2}, Lorg/isoron/uhabits/AppComponent;->getWidgetPreferences()Lorg/isoron/uhabits/preferences/WidgetPreferences;

    move-result-object v8

    iput-object v8, p0, Lorg/isoron/uhabits/widgets/HabitPickerDialog;->preferences:Lorg/isoron/uhabits/preferences/WidgetPreferences;

    .line 76
    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/HabitPickerDialog;->getIntent()Landroid/content/Intent;

    move-result-object v6

    .line 77
    .local v6, "intent":Landroid/content/Intent;
    invoke-virtual {v6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 79
    .local v3, "extras":Landroid/os/Bundle;
    if-eqz v3, :cond_0

    .line 80
    const-string v8, "appWidgetId"

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    iput-object v8, p0, Lorg/isoron/uhabits/widgets/HabitPickerDialog;->widgetId:Ljava/lang/Integer;

    .line 82
    :cond_0
    const v8, 0x7f0f00d0


    const/4 v7, 0x0


    .line 84
    .local v7, "listView":Landroid/widget/ListView;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lorg/isoron/uhabits/widgets/HabitPickerDialog;->habitIds:Ljava/util/ArrayList;

    .line 85
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 87
    .local v5, "habitNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v8, p0, Lorg/isoron/uhabits/widgets/HabitPickerDialog;->habitList:Lorg/isoron/uhabits/models/HabitList;

    invoke-virtual {v8}, Lorg/isoron/uhabits/models/HabitList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/isoron/uhabits/models/Habit;

    .line 89
    .local v4, "h":Lorg/isoron/uhabits/models/Habit;
    iget-object v9, p0, Lorg/isoron/uhabits/widgets/HabitPickerDialog;->habitIds:Ljava/util/ArrayList;

    invoke-virtual {v4}, Lorg/isoron/uhabits/models/Habit;->getId()Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    invoke-virtual {v4}, Lorg/isoron/uhabits/models/Habit;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 93
    .end local v4    # "h":Lorg/isoron/uhabits/models/Habit;
    :cond_1
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v8, 0x1090003

    invoke-direct {v0, p0, v8, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 96
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    invoke-virtual {v7, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 97
    invoke-virtual {v7, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 98
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v3, p0, Lorg/isoron/uhabits/widgets/HabitPickerDialog;->habitIds:Ljava/util/ArrayList;

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 54
    .local v1, "habitId":Ljava/lang/Long;
    iget-object v3, p0, Lorg/isoron/uhabits/widgets/HabitPickerDialog;->preferences:Lorg/isoron/uhabits/preferences/WidgetPreferences;

    iget-object v4, p0, Lorg/isoron/uhabits/widgets/HabitPickerDialog;->widgetId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, Lorg/isoron/uhabits/preferences/WidgetPreferences;->addWidget(IJ)V

    .line 56
    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/HabitPickerDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lorg/isoron/uhabits/HabitsApplication;

    .line 57
    .local v0, "app":Lorg/isoron/uhabits/HabitsApplication;
    invoke-virtual {v0}, Lorg/isoron/uhabits/HabitsApplication;->getComponent()Lorg/isoron/uhabits/AppComponent;

    move-result-object v3

    invoke-interface {v3}, Lorg/isoron/uhabits/AppComponent;->getWidgetUpdater()Lorg/isoron/uhabits/widgets/WidgetUpdater;

    move-result-object v3

    invoke-virtual {v3}, Lorg/isoron/uhabits/widgets/WidgetUpdater;->updateWidgets()V

    .line 59
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 60
    .local v2, "resultValue":Landroid/content/Intent;
    const-string v3, "appWidgetId"

    iget-object v4, p0, Lorg/isoron/uhabits/widgets/HabitPickerDialog;->widgetId:Ljava/lang/Integer;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 61
    const/4 v3, -0x1

    invoke-virtual {p0, v3, v2}, Lorg/isoron/uhabits/widgets/HabitPickerDialog;->setResult(ILandroid/content/Intent;)V

    .line 62
    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/HabitPickerDialog;->finish()V

    .line 63
    return-void
.end method
