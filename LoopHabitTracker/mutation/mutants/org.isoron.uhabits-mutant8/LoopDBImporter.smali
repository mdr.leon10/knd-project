.class public Lorg/isoron/uhabits/io/LoopDBImporter;
.super Lorg/isoron/uhabits/io/AbstractImporter;
.source "LoopDBImporter.java"


# instance fields
.field private context:Landroid/content/Context;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lorg/isoron/uhabits/models/HabitList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation runtime Lorg/isoron/uhabits/AppContext;
        .end annotation
    .end param
    .param p2, "habits"    # Lorg/isoron/uhabits/models/HabitList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p2}, Lorg/isoron/uhabits/io/AbstractImporter;-><init>(Lorg/isoron/uhabits/models/HabitList;)V

    .line 52
    iput-object p1, p0, Lorg/isoron/uhabits/io/LoopDBImporter;->context:Landroid/content/Context;

    .line 53
    return-void
.end method


# virtual methods
.method public canHandle(Ljava/io/File;)Z
    .locals 9
    .param p1, "file"    # Ljava/io/File;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 58
    invoke-static {p1}, Lorg/isoron/uhabits/io/LoopDBImporter;->isSQLite3File(Ljava/io/File;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v3

    .line 85
    :goto_0
    return v1

    .line 60
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5, v8}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 63
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x1

    .line 65
    .local v1, "canHandle":Z
    const-string v4, "select count(*) from SQLITE_MASTER where name=? or name=?"

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "Checkmarks"

    aput-object v6, v5, v3

    const-string v6, "Repetitions"

    aput-object v6, v5, v8

    const-string v4, "4190302aed3d44ee8dbaf98c8948605d"

    invoke-virtual {v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 69
    .local v0, "c":Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eq v4, v7, :cond_2

    .line 71
    :cond_1
    const-string v4, "LoopDBImporter"

    const-string v5, "Cannot handle file: tables not found"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    const/4 v1, 0x0

    .line 75
    :cond_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    move-result v4

    sget-object v5, Lorg/isoron/uhabits/BuildConfig;->databaseVersion:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-le v4, v5, :cond_3

    .line 77
    const-string v4, "LoopDBImporter"

    const-string v5, "Cannot handle file: incompatible version: %d > %d"

    new-array v6, v7, [Ljava/lang/Object;

    .line 79
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    sget-object v3, Lorg/isoron/uhabits/BuildConfig;->databaseVersion:Ljava/lang/Integer;

    aput-object v3, v6, v8

    .line 77
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    const/4 v1, 0x0

    .line 83
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 84
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_0
.end method

.method public importHabitsFromFile(Ljava/io/File;)V
    .locals 2
    .param p1, "file"    # Ljava/io/File;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    invoke-static {}, Lcom/activeandroid/ActiveAndroid;->dispose()V

    .line 92
    iget-object v1, p0, Lorg/isoron/uhabits/io/LoopDBImporter;->context:Landroid/content/Context;

    invoke-static {v1}, Lorg/isoron/uhabits/utils/DatabaseUtils;->getDatabaseFile(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 93
    .local v0, "originalDB":Ljava/io/File;
    invoke-static {p1, v0}, Lorg/isoron/uhabits/utils/FileUtils;->copy(Ljava/io/File;Ljava/io/File;)V

    .line 94
    iget-object v1, p0, Lorg/isoron/uhabits/io/LoopDBImporter;->context:Landroid/content/Context;

    invoke-static {v1}, Lorg/isoron/uhabits/utils/DatabaseUtils;->initializeActiveAndroid(Landroid/content/Context;)V

    .line 95
    return-void
.end method
