.class public abstract Lorg/isoron/uhabits/activities/BaseRootView;
.super Landroid/widget/FrameLayout;
.source "BaseRootView.java"


# instance fields
.field private final activity:Lorg/isoron/uhabits/activities/BaseActivity;

.field private final context:Landroid/content/Context;

.field private final themeSwitcher:Lorg/isoron/uhabits/activities/ThemeSwitcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 54
    iput-object p1, p0, Lorg/isoron/uhabits/activities/BaseRootView;->context:Landroid/content/Context;

    .line 55
    check-cast p1, Lorg/isoron/uhabits/activities/BaseActivity;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lorg/isoron/uhabits/activities/BaseRootView;->activity:Lorg/isoron/uhabits/activities/BaseActivity;

    .line 56
    iget-object v0, p0, Lorg/isoron/uhabits/activities/BaseRootView;->activity:Lorg/isoron/uhabits/activities/BaseActivity;

    invoke-virtual {v0}, Lorg/isoron/uhabits/activities/BaseActivity;->getComponent()Lorg/isoron/uhabits/activities/ActivityComponent;

    move-result-object v0

    invoke-interface {v0}, Lorg/isoron/uhabits/activities/ActivityComponent;->getThemeSwitcher()Lorg/isoron/uhabits/activities/ThemeSwitcher;

    move-result-object v0

    iput-object v0, p0, Lorg/isoron/uhabits/activities/BaseRootView;->themeSwitcher:Lorg/isoron/uhabits/activities/ThemeSwitcher;

    .line 57
    return-void
.end method


# virtual methods
.method public getDisplayHomeAsUp()Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return v0
.end method

.method public abstract getToolbar()Landroid/support/v7/widget/Toolbar;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public getToolbarColor()I
    .locals 4

    .prologue
    .line 69
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v1, v2, :cond_0

    iget-object v1, p0, Lorg/isoron/uhabits/activities/BaseRootView;->themeSwitcher:Lorg/isoron/uhabits/activities/ThemeSwitcher;

    invoke-virtual {v1}, Lorg/isoron/uhabits/activities/ThemeSwitcher;->isNightMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 71
    iget-object v1, p0, Lorg/isoron/uhabits/activities/BaseRootView;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d00ae

    iget-object v3, p0, Lorg/isoron/uhabits/activities/BaseRootView;->context:Landroid/content/Context;

    .line 72
    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    .line 71
    invoke-static {v1, v2, v3}, Landroid/support/v4/content/res/ResourcesCompat;->getColor(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)I

    move-result v1

    .line 76
    :goto_0
    return v1

    .line 75
    :cond_0
    new-instance v0, Lorg/isoron/uhabits/utils/StyledResources;

    iget-object v1, p0, Lorg/isoron/uhabits/activities/BaseRootView;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lorg/isoron/uhabits/utils/StyledResources;-><init>(Landroid/content/Context;)V

    .line 76
    .local v0, "res":Lorg/isoron/uhabits/utils/StyledResources;
    const v1, 0x7f0100aa

    invoke-virtual {v0, v1}, Lorg/isoron/uhabits/utils/StyledResources;->getColor(I)I

    move-result v1

    goto :goto_0
.end method

.method protected initToolbar()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 81
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_1

    .line 83
    invoke-virtual {p0}, Lorg/isoron/uhabits/activities/BaseRootView;->getToolbar()Landroid/support/v7/widget/Toolbar;

    move-result-object v1

    iget-object v2, p0, Lorg/isoron/uhabits/activities/BaseRootView;->context:Landroid/content/Context;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Lorg/isoron/uhabits/utils/InterfaceUtils;->dpToPixels(Landroid/content/Context;F)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setElevation(F)V

    .line 85
    const v1, 0x7f0f0095

    invoke-virtual {p0, v1}, Lorg/isoron/uhabits/activities/BaseRootView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 86
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 88
    :cond_0
    const v1, 0x7f0f00d5


    const/4 v0, 0x0


    .line 89
    if-eqz v0, :cond_1

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 91
    .end local v0    # "view":Landroid/view/View;
    :cond_1
    return-void
.end method
