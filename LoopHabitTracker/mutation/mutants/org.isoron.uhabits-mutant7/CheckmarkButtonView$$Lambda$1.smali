.class final synthetic Lorg/isoron/uhabits/activities/habits/list/views/CheckmarkButtonView$$Lambda$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final arg$1:Lorg/isoron/uhabits/activities/habits/list/controllers/CheckmarkButtonController;


# direct methods
.method private constructor <init>(Lorg/isoron/uhabits/activities/habits/list/controllers/CheckmarkButtonController;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/isoron/uhabits/activities/habits/list/views/CheckmarkButtonView$$Lambda$1;->arg$1:Lorg/isoron/uhabits/activities/habits/list/controllers/CheckmarkButtonController;

    return-void
.end method

.method private static get$Lambda(Lorg/isoron/uhabits/activities/habits/list/controllers/CheckmarkButtonController;)Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lorg/isoron/uhabits/activities/habits/list/views/CheckmarkButtonView$$Lambda$1;

    invoke-direct {v0, p0}, Lorg/isoron/uhabits/activities/habits/list/views/CheckmarkButtonView$$Lambda$1;-><init>(Lorg/isoron/uhabits/activities/habits/list/controllers/CheckmarkButtonController;)V

    return-object v0
.end method

.method public static lambdaFactory$(Lorg/isoron/uhabits/activities/habits/list/controllers/CheckmarkButtonController;)Landroid/view/View$OnClickListener;
    .locals 1

    new-instance v0, Lorg/isoron/uhabits/activities/habits/list/views/CheckmarkButtonView$$Lambda$1;

    invoke-direct {v0, p0}, Lorg/isoron/uhabits/activities/habits/list/views/CheckmarkButtonView$$Lambda$1;-><init>(Lorg/isoron/uhabits/activities/habits/list/controllers/CheckmarkButtonController;)V

    return-object v0
.end method


# virtual methods
.method private delay()V
    .locals 2

    .line 332
    const-wide/16 v0, 0x2710

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 335
    goto :goto_0

    .line 333
    :catch_0
    move-exception v0

    .line 334
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 336
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :goto_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .annotation runtime Ljava/lang/invoke/LambdaForm$Hidden;
    .end annotation

    iget-object v0, p0, Lorg/isoron/uhabits/activities/habits/list/views/CheckmarkButtonView$$Lambda$1;->arg$1:Lorg/isoron/uhabits/activities/habits/list/controllers/CheckmarkButtonController;
    invoke-direct/range {p0 .. p0}, Lorg/isoron/uhabits/activities/habits/list/views/CheckmarkButtonView$$Lambda$1;->delay()V

    invoke-static {v0, p1}, Lorg/isoron/uhabits/activities/habits/list/views/CheckmarkButtonView;->access$lambda$0(Lorg/isoron/uhabits/activities/habits/list/controllers/CheckmarkButtonController;Landroid/view/View;)V

    return-void
.end method
