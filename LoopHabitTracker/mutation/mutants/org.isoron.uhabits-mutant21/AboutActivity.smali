.class public Lorg/isoron/uhabits/activities/about/AboutActivity;
.super Lorg/isoron/uhabits/activities/BaseActivity;
.source "AboutActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/isoron/uhabits/activities/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 36
    invoke-super {p0, p1}, Lorg/isoron/uhabits/activities/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    new-instance v0, Lorg/isoron/uhabits/activities/about/AboutRootView;

    new-instance v2, Lorg/isoron/uhabits/intents/IntentFactory;

    invoke-direct {v2}, Lorg/isoron/uhabits/intents/IntentFactory;-><init>()V

    invoke-direct {v0, p0, v2}, Lorg/isoron/uhabits/activities/about/AboutRootView;-><init>(Landroid/content/Context;Lorg/isoron/uhabits/intents/IntentFactory;)V

    .line 38
    .local v0, "rootView":Lorg/isoron/uhabits/activities/about/AboutRootView;
    new-instance v1, Lorg/isoron/uhabits/activities/BaseScreen;

    invoke-direct {v1, p0}, Lorg/isoron/uhabits/activities/BaseScreen;-><init>(Lorg/isoron/uhabits/activities/BaseActivity;)V

    .line 39
    .local v1, "screen":Lorg/isoron/uhabits/activities/BaseScreen;
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lorg/isoron/uhabits/activities/BaseScreen;->setRootView(Lorg/isoron/uhabits/activities/BaseRootView;)V

    .line 40
    invoke-virtual {p0, v1}, Lorg/isoron/uhabits/activities/about/AboutActivity;->setScreen(Lorg/isoron/uhabits/activities/BaseScreen;)V

    .line 41
    return-void
.end method
