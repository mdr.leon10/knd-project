.class public Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;
.super Lorg/isoron/uhabits/widgets/views/HabitWidgetView;
.source "CheckmarkWidgetView.java"


# instance fields
.field private activeColor:I

.field private checkmarkValue:I

.field private label:Landroid/widget/TextView;

.field private name:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private percentage:F

.field private ring:Lorg/isoron/uhabits/activities/common/views/RingView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lorg/isoron/uhabits/widgets/views/HabitWidgetView;-><init>(Landroid/content/Context;)V

    .line 50
    invoke-direct {p0}, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->init()V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lorg/isoron/uhabits/widgets/views/HabitWidgetView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    invoke-direct {p0}, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->init()V

    .line 57
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 180
    const v0, 0x7f0f00d8


    const/4 v0, 0x0


    iput-object v0, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->ring:Lorg/isoron/uhabits/activities/common/views/RingView;

    .line 181
    const v0, 0x7f0f00d9

    invoke-virtual {p0, v0}, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->label:Landroid/widget/TextView;

    .line 183
    iget-object v0, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->ring:Lorg/isoron/uhabits/activities/common/views/RingView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->ring:Lorg/isoron/uhabits/activities/common/views/RingView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/isoron/uhabits/activities/common/views/RingView;->setIsTransparencyEnabled(Z)V

    .line 185
    :cond_0
    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 187
    const/high16 v0, 0x3f400000    # 0.75f

    iput v0, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->percentage:F

    .line 188
    const-string v0, "Wake up early"

    iput-object v0, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->name:Ljava/lang/String;

    .line 189
    const/4 v0, 0x6

    invoke-static {v0}, Lorg/isoron/uhabits/utils/ColorUtils;->getAndroidTestColor(I)I

    move-result v0

    iput v0, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->activeColor:I

    .line 190
    const/4 v0, 0x2

    iput v0, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->checkmarkValue:I

    .line 191
    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->refresh()V

    .line 193
    :cond_1
    return-void
.end method


# virtual methods
.method protected getInnerLayoutId()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 141
    const v0, 0x7f04006f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 12
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const/4 v10, 0x0

    const v9, 0x3e19999a    # 0.15f

    .line 147
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 148
    .local v6, "width":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 150
    .local v1, "height":I
    int-to-float v5, v6

    .line 151
    .local v5, "w":F
    int-to-float v7, v6

    const/high16 v8, 0x3fa00000    # 1.25f

    mul-float v0, v7, v8

    .line 152
    .local v0, "h":F
    int-to-float v7, v6

    div-float/2addr v7, v5

    int-to-float v8, v1

    div-float/2addr v8, v0

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 154
    .local v3, "scale":F
    mul-float/2addr v5, v3

    .line 155
    mul-float/2addr v0, v3

    .line 157
    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f09005b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    cmpg-float v7, v0, v7

    if-gez v7, :cond_0

    .line 158
    iget-object v7, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->ring:Lorg/isoron/uhabits/activities/common/views/RingView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lorg/isoron/uhabits/activities/common/views/RingView;->setVisibility(I)V

    .line 161
    :goto_0
    float-to-int v7, v5

    .line 162
    invoke-static {v7, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 163
    float-to-int v7, v0

    .line 164
    invoke-static {v7, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 166
    mul-float v4, v9, v0

    .line 168
    .local v4, "textSize":F
    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0900c0

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 169
    .local v2, "maxTextSize":F
    invoke-static {v4, v2}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 171
    iget-object v7, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->label:Landroid/widget/TextView;

    invoke-virtual {v7, v10, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 172
    iget-object v7, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->ring:Lorg/isoron/uhabits/activities/common/views/RingView;

    invoke-virtual {v7, v4}, Lorg/isoron/uhabits/activities/common/views/RingView;->setTextSize(F)V

    .line 173
    iget-object v7, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->ring:Lorg/isoron/uhabits/activities/common/views/RingView;

    mul-float v8, v9, v4

    invoke-virtual {v7, v8}, Lorg/isoron/uhabits/activities/common/views/RingView;->setThickness(F)V

    .line 175
    invoke-super {p0, p1, p2}, Lorg/isoron/uhabits/widgets/views/HabitWidgetView;->onMeasure(II)V

    .line 176
    return-void

    .line 159
    .end local v2    # "maxTextSize":F
    .end local v4    # "textSize":F
    :cond_0
    iget-object v7, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->ring:Lorg/isoron/uhabits/activities/common/views/RingView;

    invoke-virtual {v7, v10}, Lorg/isoron/uhabits/activities/common/views/RingView;->setVisibility(I)V

    goto :goto_0
.end method

.method public refresh()V
    .locals 9

    .prologue
    const v5, 0x7f0800cd

    const v8, 0x7f010019

    const v7, 0x7f010002

    const/4 v6, 0x0

    .line 61
    iget-object v4, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->backgroundPaint:Landroid/graphics/Paint;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->frame:Landroid/view/ViewGroup;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->ring:Lorg/isoron/uhabits/activities/common/views/RingView;

    if-nez v4, :cond_1

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    new-instance v2, Lorg/isoron/uhabits/utils/StyledResources;

    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Lorg/isoron/uhabits/utils/StyledResources;-><init>(Landroid/content/Context;)V

    .line 69
    .local v2, "res":Lorg/isoron/uhabits/utils/StyledResources;
    iget v4, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->checkmarkValue:I

    packed-switch v4, :pswitch_data_0

    .line 95
    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0800d1

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 96
    .local v3, "text":Ljava/lang/String;
    invoke-virtual {v2, v7}, Lorg/isoron/uhabits/utils/StyledResources;->getColor(I)I

    move-result v0

    .line 97
    .local v0, "bgColor":I
    invoke-virtual {v2, v8}, Lorg/isoron/uhabits/utils/StyledResources;->getColor(I)I

    move-result v1

    .line 99
    .local v1, "fgColor":I
    invoke-virtual {p0, v6}, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->setShadowAlpha(I)V

    .line 100
    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->rebuildBackground()V

    .line 105
    :goto_1
    iget-object v4, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->ring:Lorg/isoron/uhabits/activities/common/views/RingView;

    iget v5, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->percentage:F

    invoke-virtual {v4, v5}, Lorg/isoron/uhabits/activities/common/views/RingView;->setPercentage(F)V

    .line 106
    iget-object v4, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->ring:Lorg/isoron/uhabits/activities/common/views/RingView;

    invoke-virtual {v4, v1}, Lorg/isoron/uhabits/activities/common/views/RingView;->setColor(I)V

    .line 107
    iget-object v4, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->ring:Lorg/isoron/uhabits/activities/common/views/RingView;

    invoke-virtual {v4, v0}, Lorg/isoron/uhabits/activities/common/views/RingView;->setBackgroundColor(I)V

    .line 108
    iget-object v4, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->ring:Lorg/isoron/uhabits/activities/common/views/RingView;

    invoke-virtual {v4, v3}, Lorg/isoron/uhabits/activities/common/views/RingView;->setText(Ljava/lang/String;)V

    .line 110
    iget-object v4, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->label:Landroid/widget/TextView;

    iget-object v5, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v4, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->label:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 113
    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->requestLayout()V

    .line 114
    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->postInvalidate()V

    goto :goto_0

    .line 72
    .end local v0    # "bgColor":I
    .end local v1    # "fgColor":I
    .end local v3    # "text":Ljava/lang/String;
    :pswitch_0
    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 73
    .restart local v3    # "text":Ljava/lang/String;
    iget v0, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->activeColor:I

    .line 74
    .restart local v0    # "bgColor":I
    const v4, 0x7f010009

    invoke-virtual {v2, v4}, Lorg/isoron/uhabits/utils/StyledResources;->getColor(I)I

    move-result v1

    .line 76
    .restart local v1    # "fgColor":I
    const/16 v4, 0x4f

    invoke-virtual {p0, v4}, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->setShadowAlpha(I)V

    .line 77
    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->rebuildBackground()V

    .line 79
    iget-object v4, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->backgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 80
    iget-object v4, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->frame:Landroid/view/ViewGroup;

    iget-object v5, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->background:Landroid/graphics/drawable/InsetDrawable;

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 84
    .end local v0    # "bgColor":I
    .end local v1    # "fgColor":I
    .end local v3    # "text":Ljava/lang/String;
    :pswitch_1
    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 85
    .restart local v3    # "text":Ljava/lang/String;
    invoke-virtual {v2, v7}, Lorg/isoron/uhabits/utils/StyledResources;->getColor(I)I

    move-result v0

    .line 86
    .restart local v0    # "bgColor":I
    invoke-virtual {v2, v8}, Lorg/isoron/uhabits/utils/StyledResources;->getColor(I)I

    move-result v1

    .line 88
    .restart local v1    # "fgColor":I
    invoke-virtual {p0, v6}, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->setShadowAlpha(I)V

    .line 89
    invoke-virtual {p0}, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->rebuildBackground()V

    goto :goto_1

    .line 69
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setActiveColor(I)V
    .locals 0
    .param p1, "activeColor"    # I

    .prologue
    .line 119
    iput p1, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->activeColor:I

    .line 120
    return-void
.end method

.method public setCheckmarkValue(I)V
    .locals 0
    .param p1, "checkmarkValue"    # I

    .prologue
    .line 124
    iput p1, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->checkmarkValue:I

    .line 125
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 129
    iput-object p1, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->name:Ljava/lang/String;

    .line 130
    return-void
.end method

.method public setPercentage(F)V
    .locals 0
    .param p1, "percentage"    # F

    .prologue
    .line 134
    iput p1, p0, Lorg/isoron/uhabits/widgets/views/CheckmarkWidgetView;->percentage:F

    .line 135
    return-void
.end method
