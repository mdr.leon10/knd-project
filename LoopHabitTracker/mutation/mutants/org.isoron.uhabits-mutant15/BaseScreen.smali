.class public Lorg/isoron/uhabits/activities/BaseScreen;
.super Ljava/lang/Object;
.source "BaseScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/isoron/uhabits/activities/BaseScreen$ActionModeWrapper;
    }
.end annotation


# static fields
.field public static final REQUEST_CREATE_DOCUMENT:I = 0x1


# instance fields
.field protected activity:Lorg/isoron/uhabits/activities/BaseActivity;

.field private rootView:Lorg/isoron/uhabits/activities/BaseRootView;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private selectionMenu:Lorg/isoron/uhabits/activities/BaseSelectionMenu;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private snackbar:Landroid/support/design/widget/Snackbar;


# direct methods
.method public constructor <init>(Lorg/isoron/uhabits/activities/BaseActivity;)V
    .locals 0
    .param p1, "activity"    # Lorg/isoron/uhabits/activities/BaseActivity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lorg/isoron/uhabits/activities/BaseScreen;->activity:Lorg/isoron/uhabits/activities/BaseActivity;

    .line 69
    return-void
.end method

.method static synthetic access$100(Lorg/isoron/uhabits/activities/BaseScreen;)Lorg/isoron/uhabits/activities/BaseSelectionMenu;
    .locals 1
    .param p0, "x0"    # Lorg/isoron/uhabits/activities/BaseScreen;

    .prologue
    .line 52
    iget-object v0, p0, Lorg/isoron/uhabits/activities/BaseScreen;->selectionMenu:Lorg/isoron/uhabits/activities/BaseSelectionMenu;

    return-object v0
.end method

.method static synthetic access$lambda$0(Lorg/isoron/uhabits/activities/BaseScreen;)V
    .locals 0

    invoke-direct {p0}, Lorg/isoron/uhabits/activities/BaseScreen;->lambda$invalidateToolbar$0()V

    return-void
.end method

.method public static getDefaultActionBarColor(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 108
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v1, v2, :cond_0

    .line 110
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d00ae

    .line 111
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    .line 110
    invoke-static {v1, v2, v3}, Landroid/support/v4/content/res/ResourcesCompat;->getColor(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)I

    move-result v1

    .line 116
    :goto_0
    return v1

    .line 115
    :cond_0
    new-instance v0, Lorg/isoron/uhabits/utils/StyledResources;

    invoke-direct {v0, p0}, Lorg/isoron/uhabits/utils/StyledResources;-><init>(Landroid/content/Context;)V

    .line 116
    .local v0, "res":Lorg/isoron/uhabits/utils/StyledResources;
    const v1, 0x7f0100aa

    invoke-virtual {v0, v1}, Lorg/isoron/uhabits/utils/StyledResources;->getColor(I)I

    move-result v1

    goto :goto_0
.end method

.method private synthetic lambda$invalidateToolbar$0()V
    .locals 4

    .prologue
    .line 134
    iget-object v3, p0, Lorg/isoron/uhabits/activities/BaseScreen;->rootView:Lorg/isoron/uhabits/activities/BaseRootView;

    invoke-virtual {v3}, Lorg/isoron/uhabits/activities/BaseRootView;->getToolbar()Landroid/support/v7/widget/Toolbar;

    move-result-object v2

    .line 135
    .local v2, "toolbar":Landroid/support/v7/widget/Toolbar;
    iget-object v3, p0, Lorg/isoron/uhabits/activities/BaseScreen;->activity:Lorg/isoron/uhabits/activities/BaseActivity;

    invoke-virtual {v3, v2}, Lorg/isoron/uhabits/activities/BaseActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 136
    iget-object v3, p0, Lorg/isoron/uhabits/activities/BaseScreen;->activity:Lorg/isoron/uhabits/activities/BaseActivity;

    invoke-virtual {v3}, Lorg/isoron/uhabits/activities/BaseActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 137
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-nez v0, :cond_0

    .line 144
    :goto_0
    return-void

    .line 139
    :cond_0
    iget-object v3, p0, Lorg/isoron/uhabits/activities/BaseScreen;->rootView:Lorg/isoron/uhabits/activities/BaseRootView;

    invoke-virtual {v3}, Lorg/isoron/uhabits/activities/BaseRootView;->getDisplayHomeAsUp()Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 141
    iget-object v3, p0, Lorg/isoron/uhabits/activities/BaseScreen;->rootView:Lorg/isoron/uhabits/activities/BaseRootView;

    invoke-virtual {v3}, Lorg/isoron/uhabits/activities/BaseRootView;->getToolbarColor()I

    move-result v1

    .line 142
    .local v1, "color":I
    invoke-direct {p0, v0, v1}, Lorg/isoron/uhabits/activities/BaseScreen;->setActionBarColor(Landroid/support/v7/app/ActionBar;I)V

    .line 143
    invoke-direct {p0, v1}, Lorg/isoron/uhabits/activities/BaseScreen;->setStatusBarColor(I)V

    goto :goto_0
.end method

.method private setActionBarColor(Landroid/support/v7/app/ActionBar;I)V
    .locals 1
    .param p1, "actionBar"    # Landroid/support/v7/app/ActionBar;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "color"    # I

    .prologue
    .line 260
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, p2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 261
    .local v0, "drawable":Landroid/graphics/drawable/ColorDrawable;
    invoke-virtual {p1, v0}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 262
    return-void
.end method

.method private setStatusBarColor(I)V
    .locals 3
    .param p1, "baseColor"    # I

    .prologue
    .line 266
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v1, v2, :cond_0

    .line 270
    :goto_0
    return-void

    .line 268
    :cond_0
    const/high16 v1, -0x1000000

    const/high16 v2, 0x3f400000    # 0.75f

    invoke-static {p1, v1, v2}, Lorg/isoron/uhabits/utils/ColorUtils;->mixColors(IIF)I

    move-result v0

    .line 269
    .local v0, "darkerColor":I
    iget-object v1, p0, Lorg/isoron/uhabits/activities/BaseScreen;->activity:Lorg/isoron/uhabits/activities/BaseActivity;

    invoke-virtual {v1}, Lorg/isoron/uhabits/activities/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    goto :goto_0
.end method

.method public static setupActionBarColor(Landroid/support/v7/app/AppCompatActivity;I)V
    .locals 8
    .param p0, "activity"    # Landroid/support/v7/app/AppCompatActivity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "color"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/16 v7, 0x8

    .line 76
    const v5, 0x7f0f008f

    invoke-virtual {p0, v5}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/Toolbar;

    .line 77
    .local v3, "toolbar":Landroid/support/v7/widget/Toolbar;
    if-nez v3, :cond_1

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    invoke-virtual {p0, v3}, Landroid/support/v7/app/AppCompatActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 81
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 82
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v0, :cond_0

    .line 84
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 87
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 88
    .local v2, "drawable":Landroid/graphics/drawable/ColorDrawable;
    invoke-virtual {v0, v2}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 90
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x15

    if-lt v5, v6, :cond_0

    .line 92
    const/high16 v5, -0x1000000

    const/high16 v6, 0x3f400000    # 0.75f

    invoke-static {p1, v5, v6}, Lorg/isoron/uhabits/utils/ColorUtils;->mixColors(IIF)I

    move-result v1

    .line 93
    .local v1, "darkerColor":I
    invoke-virtual {p0}, Landroid/support/v7/app/AppCompatActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 95
    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {p0, v5}, Lorg/isoron/uhabits/utils/InterfaceUtils;->dpToPixels(Landroid/content/Context;F)F

    move-result v5

    invoke-virtual {v3, v5}, Landroid/support/v7/widget/Toolbar;->setElevation(F)V

    .line 97
    const v5, 0x7f0f0095

    invoke-virtual {p0, v5}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 98
    .local v4, "view":Landroid/view/View;
    if-eqz v4, :cond_2

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 100
    :cond_2
    const v5, 0x7f0f00d5

    invoke-virtual {p0, v5}, Landroid/support/v7/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 101
    if-eqz v4, :cond_0

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public invalidate()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lorg/isoron/uhabits/activities/BaseScreen;->rootView:Lorg/isoron/uhabits/activities/BaseRootView;

    if-nez v0, :cond_0

    .line 127
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v0, p0, Lorg/isoron/uhabits/activities/BaseScreen;->rootView:Lorg/isoron/uhabits/activities/BaseRootView;

    invoke-virtual {v0}, Lorg/isoron/uhabits/activities/BaseRootView;->invalidate()V

    goto :goto_0
.end method

.method public invalidateToolbar()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lorg/isoron/uhabits/activities/BaseScreen;->rootView:Lorg/isoron/uhabits/activities/BaseRootView;

    if-nez v0, :cond_0

    .line 145
    :goto_0
    return-void

    .line 133
    :cond_0
    iget-object v0, p0, Lorg/isoron/uhabits/activities/BaseScreen;->activity:Lorg/isoron/uhabits/activities/BaseActivity;

    invoke-static {p0}, Lorg/isoron/uhabits/activities/BaseScreen$$Lambda$1;->lambdaFactory$(Lorg/isoron/uhabits/activities/BaseScreen;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/isoron/uhabits/activities/BaseActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 160
    return-void
.end method

.method public setMenu(Lorg/isoron/uhabits/activities/BaseMenu;)V
    .locals 1
    .param p1, "menu"    # Lorg/isoron/uhabits/activities/BaseMenu;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 172
    iget-object v0, p0, Lorg/isoron/uhabits/activities/BaseScreen;->activity:Lorg/isoron/uhabits/activities/BaseActivity;

    invoke-virtual {v0, p1}, Lorg/isoron/uhabits/activities/BaseActivity;->setBaseMenu(Lorg/isoron/uhabits/activities/BaseMenu;)V

    .line 173
    return-void
.end method

.method public setRootView(Lorg/isoron/uhabits/activities/BaseRootView;)V
    .locals 1
    .param p1, "rootView"    # Lorg/isoron/uhabits/activities/BaseRootView;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 182
    iput-object p1, p0, Lorg/isoron/uhabits/activities/BaseScreen;->rootView:Lorg/isoron/uhabits/activities/BaseRootView;

    .line 183
    iget-object v0, p0, Lorg/isoron/uhabits/activities/BaseScreen;->activity:Lorg/isoron/uhabits/activities/BaseActivity;

    invoke-virtual {v0, p1}, Lorg/isoron/uhabits/activities/BaseActivity;->setContentView(Landroid/view/View;)V

    .line 184
    if-nez p1, :cond_0

    .line 187
    :goto_0
    return-void

    .line 186
    :cond_0
    invoke-virtual {p0}, Lorg/isoron/uhabits/activities/BaseScreen;->invalidateToolbar()V

    goto :goto_0
.end method

.method public setSelectionMenu(Lorg/isoron/uhabits/activities/BaseSelectionMenu;)V
    .locals 0
    .param p1, "menu"    # Lorg/isoron/uhabits/activities/BaseSelectionMenu;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 196
    iput-object p1, p0, Lorg/isoron/uhabits/activities/BaseScreen;->selectionMenu:Lorg/isoron/uhabits/activities/BaseSelectionMenu;

    .line 197
    return-void
.end method

.method public showMessage(Ljava/lang/Integer;)V
    .locals 5
    .param p1, "stringId"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    const/4 v4, -0x1

    .line 206
    if-eqz p1, :cond_0

    iget-object v2, p0, Lorg/isoron/uhabits/activities/BaseScreen;->rootView:Lorg/isoron/uhabits/activities/BaseRootView;

    if-nez v2, :cond_1

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 207
    :cond_1
    iget-object v2, p0, Lorg/isoron/uhabits/activities/BaseScreen;->snackbar:Landroid/support/design/widget/Snackbar;

    if-nez v2, :cond_2

    .line 209
    iget-object v2, p0, Lorg/isoron/uhabits/activities/BaseScreen;->rootView:Lorg/isoron/uhabits/activities/BaseRootView;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3, v4}, Landroid/support/design/widget/Snackbar;->make(Landroid/view/View;II)Landroid/support/design/widget/Snackbar;

    move-result-object v2

    iput-object v2, p0, Lorg/isoron/uhabits/activities/BaseScreen;->snackbar:Landroid/support/design/widget/Snackbar;

    .line 210
    const v1, 0x7f0f00ac

    .line 211
    .local v1, "tvId":I
    iget-object v2, p0, Lorg/isoron/uhabits/activities/BaseScreen;->snackbar:Landroid/support/design/widget/Snackbar;

    invoke-virtual {v2}, Landroid/support/design/widget/Snackbar;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 212
    .local v0, "tv":Landroid/widget/TextView;
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 215
    .end local v0    # "tv":Landroid/widget/TextView;
    .end local v1    # "tvId":I
    :goto_1
    iget-object v2, p0, Lorg/isoron/uhabits/activities/BaseScreen;->snackbar:Landroid/support/design/widget/Snackbar;

    invoke-virtual {v2}, Landroid/support/design/widget/Snackbar;->show()V

    goto :goto_0

    .line 214
    :cond_2
    iget-object v2, p0, Lorg/isoron/uhabits/activities/BaseScreen;->snackbar:Landroid/support/design/widget/Snackbar;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/support/design/widget/Snackbar;->setText(I)Landroid/support/design/widget/Snackbar;

    goto :goto_1
.end method

.method public showSendEmailScreen(IILjava/lang/String;)V
    .locals 6
    .param p1, "toId"    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p2, "subjectId"    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p3, "content"    # Ljava/lang/String;

    .prologue
    .line 222
    iget-object v3, p0, Lorg/isoron/uhabits/activities/BaseScreen;->activity:Lorg/isoron/uhabits/activities/BaseActivity;

    invoke-virtual {v3, p1}, Lorg/isoron/uhabits/activities/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 223
    .local v2, "to":Ljava/lang/String;
    iget-object v3, p0, Lorg/isoron/uhabits/activities/BaseScreen;->activity:Lorg/isoron/uhabits/activities/BaseActivity;

    invoke-virtual {v3, p2}, Lorg/isoron/uhabits/activities/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 225
    .local v1, "subject":Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 226
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "android.intent.action.SEND"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 227
    const-string v3, "message/rfc822"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 228
    const-string v3, "d8680edb8aca486d840994e502c59cd1"


    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 229
    const-string v3, "android.intent.extra.SUBJECT"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 230
    const-string v3, "android.intent.extra.TEXT"

    invoke-virtual {v0, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 231
    iget-object v3, p0, Lorg/isoron/uhabits/activities/BaseScreen;->activity:Lorg/isoron/uhabits/activities/BaseActivity;

    invoke-virtual {v3, v0}, Lorg/isoron/uhabits/activities/BaseActivity;->startActivity(Landroid/content/Intent;)V

    .line 232
    return-void
.end method

.method public showSendFileScreen(Ljava/lang/String;)V
    .locals 5
    .param p1, "archiveFilename"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 236
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 237
    .local v0, "file":Ljava/io/File;
    iget-object v3, p0, Lorg/isoron/uhabits/activities/BaseScreen;->activity:Lorg/isoron/uhabits/activities/BaseActivity;

    const-string v4, "org.isoron.uhabits"

    invoke-static {v3, v4, v0}, Landroid/support/v4/content/FileProvider;->getUriForFile(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 239
    .local v1, "fileUri":Landroid/net/Uri;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 240
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "android.intent.action.SEND"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 241
    const-string v3, "application/zip"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 242
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 243
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 244
    iget-object v3, p0, Lorg/isoron/uhabits/activities/BaseScreen;->activity:Lorg/isoron/uhabits/activities/BaseActivity;

    invoke-virtual {v3, v2}, Lorg/isoron/uhabits/activities/BaseActivity;->startActivity(Landroid/content/Intent;)V

    .line 245
    return-void
.end method

.method public startSelection()V
    .locals 3

    .prologue
    .line 255
    iget-object v0, p0, Lorg/isoron/uhabits/activities/BaseScreen;->activity:Lorg/isoron/uhabits/activities/BaseActivity;

    new-instance v1, Lorg/isoron/uhabits/activities/BaseScreen$ActionModeWrapper;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lorg/isoron/uhabits/activities/BaseScreen$ActionModeWrapper;-><init>(Lorg/isoron/uhabits/activities/BaseScreen;Lorg/isoron/uhabits/activities/BaseScreen$1;)V

    invoke-virtual {v0, v1}, Lorg/isoron/uhabits/activities/BaseActivity;->startSupportActionMode(Landroid/support/v7/view/ActionMode$Callback;)Landroid/support/v7/view/ActionMode;

    .line 256
    return-void
.end method
