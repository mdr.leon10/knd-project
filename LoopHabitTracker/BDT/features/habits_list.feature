Feature: Habits list
  Scenario: As a user I want to create a new habit in my list for tracking the progress of one of my objectives
    Given I press "next"
    Given I press "next"
    Given I press "done"
    Given I press view with id "actionAdd"
    And I enter text "Test" into field with id "tvName"
    And I enter text "Is Test?" into field with id "tvDescription"
    And I press "spinner"
    And I press "Semanalmente"
    And I press "tvReminderTime"
    And I press "done_button"
    And I press "buttonSave"
    Then I should see "Test"

  Scenario: As I user I want to be reminded that the name of a habit cant be blank when I try to create one with no name.
    Given I press "actionAdd"
    And I press "buttonSave"
    Then I should see "El nombre no puede quedar en blanco."

  Scenario: As I user I want to edit a habit to correct mistakes when I created it.
    Given I long press "Test"
    Given I press "action_edit_habit"
    And I clear input field with id "tvName"
    And  I enter text "New Name" into field with id "tvName"
    Then I see the text "New Name"

  Scenario: As I user I want to add a new calendar entry.
    Given I press "Test"
    And I press "Edit"
    And  I press "25"
    And I press "Ok"
    Then I see the text "Best streaks"

  Scenario: As I user want to erase and existing habit
    Given I long press "Test"
    And I press "actionMenu"
    And I press "action_delete"
    And I press "buttonAccept"
    Then I should not see "Test"

