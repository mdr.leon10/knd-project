var express = require('express')
var resemble = require('resemblejs')
var fs = require('file-system')
var app = express()
var shell = require('shelljs');

var num = Math.floor(Math.random() * 100) + 1;
var case1 = 'adb shell monkey -p org.isoron.uhabits -s '+ num +' --throttle 120 -v -v -v 20000';
var case2 = 'adb shell monkey -p org.isoron.uhabits -s '+ num +' --throttle 120 --pct-touch 100 -v -v -v 20000';
var case3 = 'adb shell monkey -p org.isoron.uhabits -s '+ num +' --throttle 120 --pct-syskeys 0 -v -v -v 20000';

app.get('/', (req) => {

    console.log('Starting random tests')

    if(num%7=0){
        shell.exec(case3);
    }else{
        if (num%5=0){
            shell.exec(case2);
        }else{
            shell.exec(case1);
        }
    }
});
app.listen(3000, () => {
    console.log('Running random tests')
})