const express = require('express')
const shell = require('shelljs')
var app = express()

app.get('/',(req,res)=>{
    console.log('Inicio mutantion testing MutAPK k9mail')
    shell.exec('rm -R mutants/*');
    shell.exec('rm -R reportes/*');
    console.log('inicio crear mutantes');
    shell.exec('java -jar target/MutAPK-0.0.1.jar /k9mail.apk com.fsck.k9 mutants/ extra/ . false 3');
	console.log('final mutantes Creados');
	

    for(var i = 1; i <= 3; i++){
        console.log('inicio instalacion apk mutante'+i);
        shell.exec('adb uninstall com.fsck.k9');
        shell.exec('adb install mutants/com.fsck.k9-mutant'+i+'/k9mail-aligned-debugSigned.apk');
        console.log('final instalacion apk mutante'+i);
        console.log('inicio pruebas monkey para mutante'+i);
        shell.exec('adb shell monkey -p com.fsck.k9 -v 1000 >> reportes/rMutante'+i+'.txt');
        console.log('final pruebas monkey para mutante'+i);
        console.log('generacion reporte rMutante'+i+'.txt');
    }
    console.log('Fin mutantion testing MutAPK k9mail')
  res.send('done')
})
app.listen(3002, () => {
    console.log('Running random tests')
})