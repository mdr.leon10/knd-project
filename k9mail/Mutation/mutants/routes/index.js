var express = require('express');
var shell = require('shelljs');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {

 console.log('Starting mutantion testing')
    
    shell.exec('cp -R /home/michel/PruebasAuto/knd-project/k9mail/Mutation/k-9Original/* /home/michel/PruebasAuto/knd-project/k9mail/Mutation/k-9');
    console.log('Copia Realizada');
    shell.exec('java -jar /home/michel/PruebasAuto/knd-project/k9mail/Mutation/MDroidPlus/target/MDroidPlus-1.0.0.jar /home/michel/PruebasAuto/knd-project/k9mail/Mutation/MDroidPlus/combined.jar /home/michel/PruebasAuto/knd-project/k9mail/Mutation/k-9/app/k9mail/src/main/ com.fsck.k9 /home/michel/PruebasAuto/knd-project/k9mail/Mutation/MDroidPlus/tmp/mutants/ /home/michel/PruebasAuto/knd-project/k9mail/Mutation/MDroidPlus/ true');
	console.log('mutantes Creados');

    for(var i = 1; i <= 10; i++){
        shell.exec('cp -R /home/michel/PruebasAuto/knd-project/k9mail/Mutation/MDroidPlus/tmp/mutants/com.fsck.k9-mutant'+i+'/* /home/michel/PruebasAuto/knd-project/k9mail/Mutation/k-9/app/k9mail/src/main');
        console.log('Copia mutante '+i);
        
        shell.exec('cd /home/michel/PruebasAuto/knd-project/k9mail/Mutation/k-9')

        shell.exec('gradle installDebug');
        
        shell.exec('adb shell monkey -p com.fsck.k9.debug -v 10 >> rMutante'+i);
        //Guardar resultado del monkey
    }


  res.render('index', { title: 'Express' });
});

module.exports = router;
