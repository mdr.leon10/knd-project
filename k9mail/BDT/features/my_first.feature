Feature: Envio de correos

  Scenario: 1) As a user register your email
    When I press the "Siguiente" button
    Given I enter text "appstesting970@gmail.com" into field with id "account_email"
    And I enter text "Qazokm2018*" into field with id "account_password"
    And I press "Siguiente"
    And I should see "Terminado"
    And I enter text "mi correo" into field with id "account_name"
    And I press "Terminado"
    And I wait for 3 seconds
    And I should see "Registro de cambios"
    Then I press "Aceptar"

Scenario: 2) As a user send a email
    When I press the menu key
    And I press "Redactar"
    And I enter text "a@a.com" into field with id "to"
    And I enter text "el asunto de esta prueba" into field with id "subject"
    And I enter text "cuerpo del correo de esta prueba" into field with id "message_content"
    When I press "Enviar"
    And I wait for 10 seconds
    
