# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.shortcuts import render

# Create your views here.

@api_view(['GET', 'POST'])
def test(request):
    if request.method == 'POST':
        app = request.data['app']
        test = request.data['test']
        amount = request.data['amount']
        device = request.data['device']
        vm = request.data['vm']

        response_test = inittest(app, test, amount, device, vm)

        return Response(response_test)
    return Response({"message": "Hello, world!"})

def inittest(app, test, amount, device, vm):
    import os
    import subprocess

    print('Lista de dispositivos activos')
    device_list = "gmsaas instances list"
    result_device = subprocess.check_output(device_list, shell=True)
    result_device = str(result_device).split(' ')
    estado = False
    for i in result_device:
        if (len(i.strip('-')) > 30):
            if estado == False:
                print('Ya ha sido creado un emulador')
                i = i.split('\n')
                device = i[1]
            estado = True

    if estado == False:

        if(device == 'SGS9'):
            print('Creando un emulador Samsung Galaxy S9 - ANDROID: 9.0 - SCREEN: 1440 x 2960 dpi 560')
            emulator_create = "gmsaas instances start e20da1a3-313c-434a-9d43-7268b12fee08 SGS9"
        elif(device == 'GPC'):
            print('Creando un emulador Google Pixel C - ANDROID: 8.0 - SCREEN: 2560 x 1800 dpi 320')
            emulator_create = "gmsaas instances start 3e1aa8be-b625-4622-8705-c5a0c69fa687 GPC"
        try:
            device = subprocess.check_output(emulator_create, shell=True)
            print(device)
        except ValueError:
            print('No pudo crearse este dispositivo')

    result_device = subprocess.check_output(device_list, shell=True)
    if (result_device.isalnum == True):
        print('No hay dispositivos creados')

    print('Conectando disposivo por ADB')
    device = str(device)
    device = device.replace("b'", "")
    device = device.replace("\\n'", "")
    adb = ("gmsaas instances adbconnect " + str(device))
    result_adb = subprocess.check_output(adb, shell=True)
    os.system('adb shell "setprop persist.sys.language es; setprop persist.sys.country CO; setprop ctl.restart zygote"')
    print(result_adb)


    if (test == 'bdt'):
        print('Ejecutando pruebas BDT')
        if (vm == 'GCP'):
            if (app == 'k9-mail'):
                bdt_test = "cd /home/cmartinezbedoya/test && calabash-android run k9mail-release.apk"
            elif (app == 'habitica'):
                bdt_test = "cd /home/cmartinezbedoya/knd-project/LoopHabitTracker/BDT && calabash-android run LoopHabit.apk"
        else:
            bdt_test = "cd /Users/ChrisMartin/Testing/knd-project/k9mail/BDT/ && calabash-android run /Users/ChrisMartin/Testing/knd-project/k9mail/BDT/k9mail-release.apk --format AllureCucumber::CucumberFormatter --out /Users/ChrisMartin/Testing/knd-project/k9mail/BDT/reports"

        print(os.system(bdt_test))
    elif(test == 'random'):
        print('Ejecutando pruebas Random')
        random_test = "cd /home/cmartinezbedoya/test && calabash-android run k9mail-release.apk"
        print(os.system(random_test))
        print(os.system("adb shell monkey -p com.fsck.k9 -v " + str(amount)))
    elif (test == 'e2e'):
        print('Ejecutando pruebas E2E')
        if (vm == 'GCP'):
            e2e_test = "cd /home/cmartinezbedoya/e2e && calabash-android run k9mail-release.apk"
        else:
            e2e_test = "cd /Users/ChrisMartin/Testing/knd-project/k9mail/BDT/ && calabash-android run /Users/ChrisMartin/Testing/knd-project/k9mail/BDT/k9mail-release.apk --format AllureCucumber::CucumberFormatter --out /Users/ChrisMartin/Testing/knd-project/k9mail/BDT/reports"

        print(os.system(e2e_test))
    subprocess.check_output("gmsaas instances stop " + device, shell=True)
    return device
