from django.urls import path
from restmobile.views import test


urlpatterns = [
    path('test/', test, name='test-view'),
]