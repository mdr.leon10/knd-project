function loadScript(callback) {
  var s = document.createElement('script');
  s.src = 'https://rawgithub.com/marmelab/gremlins.js/master/gremlins.min.js';
  if (s.addEventListener) {
    s.addEventListener('load', callback, false);
  } else if (s.readyState) {
    s.onreadystatechange = callback;
  }
  document.body.appendChild(s);
}

function unleashGremlins(ttl, callback) {
  function stop() {
    horde.stop();
    callback();
  }
  var horde = window.gremlins.createHorde()
    .gremlin(window.gremlins.species.clicker().clickTypes(['click']).canClick((element)=>{
      return (element.tagName == "A" || element.tagName == "BUTTON" || element.class == ".create-task-btn");
    }))
    .gremlin(window.gremlins.species.formFiller().canFillElement((element)=>{
      return (element.tagName == "INPUT" || element.type == "text" || element.type == "password" || element.type == "email");
    }))
    .gremlin(window.gremlins.species.toucher())
    .gremlin(window.gremlins.species.scroller());
  horde.seed(Math.floor(Math.random()*999999));

  horde.strategy(window.gremlins.strategies.distribution()
    .delay(1)
    .distribution([0.4, 0.4, 0.1,0.1])
  )

  horde.after(callback);
  window.onbeforeunload = stop;
  setTimeout(stop, ttl);
  horde.unleash();
}

describe('Monkey testing with gremlins ', function () {

  it('it should not raise any error', function () {
    browser.url('/');
    
    browser.element('.login-button').click()
    
    var username = browser.element('input[id="usernameInput"]')
    username.click()
    username.keys("KND-Testing")
    
    var pwd = browser.element('input[id="passwordInput"]')
    pwd.click()
    pwd.keys("Habbo123")

    browser.element('div[class="btn btn-info"]').click()

    if(browser.element(".start-day").isVisible()){
      browser.element(".start-day").click()
    }
    if(browser.element('div[id="new-stuff___BV_modal_body_"]').isVisible()){
      browser.element('div[id="new-stuff___BV_modal_body_"]').element('.btn-warning').click()
    }
    browser.timeoutsAsyncScript(60000);
    browser.executeAsync(loadScript);

    browser.timeoutsAsyncScript(60000);
    browser.executeAsync(unleashGremlins, 50000);
  });

  afterAll(function () {
    browser.log('browser').value.forEach(function (log) {
      browser.logger.info(log.message);
    });
  });

});