var express = require('express')
var resemble = require('resemblejs')
var fs = require('file-system')
var app = express()
var shell = require('shelljs');


app.get('/', (req, res) => {
    console.log('Starting regression tests')
    console.log('Taking new screenshots')
    shell.exec('rm -rf older');
    shell.exec('mkdir older');
    shell.exec('cp -r ./recent/ ./older/')
    shell.exec('rm -rf recent');
    shell.exec('mkdir recent');
    shell.exec('rm -rf ./cypress/screenshots')
    shell.exec('./node_modules/.bin/cypress run')
    shell.exec('cp -r ./cypress/screenshots/test-specs.js/ ./recent')
    var responseData = {
        'habitica': undefined,
        'crearHabito': undefined,
        'modificarHabito': undefined,
        'eliminarHabito': undefined,
        'crearDaily': undefined,
        'modificarDaily': undefined,
        'eliminarDaily': undefined,
        'crearTODO': undefined,
        'modificarTODO': undefined,
        'eliminarTODO': undefined
    }
    resemble('./recent/habitica.png')
        .compareTo('./older/habitica.png')
        .onComplete(function (data) {
            console.log('Starting ResembleJS')
            fs.writeFile("./differences/habitica.png", data.getBuffer());
            responseData.habitica = data;
            resemble('./recent/crearHabito.png')
                .compareTo('./older/crearHabito.png')
                .onComplete(function (data) {
                    fs.writeFile("./differences/crearHabito.png", data.getBuffer());
                    responseData.crearHabito = data
                    resemble('./recent/modificarHabito.png')
                        .compareTo('./older/modificarHabito.png')
                        .onComplete(function (data) {
                            fs.writeFile("./differences/modificarHabito.png", data.getBuffer());
                            responseData.modificarHabito = data
                            resemble('./recent/eliminarHabito.png')
                                .compareTo('./older/eliminarHabito.png')
                                .onComplete(function (data) {
                                    fs.writeFile("./differences/eliminarHabito.png", data.getBuffer());
                                    responseData.eliminarHabito = data
                                    resemble('./recent/crearDaily.png')
                                        .compareTo('./older/crearDaily.png')
                                        .onComplete(function (data) {
                                            fs.writeFile("./differences/crearDaily.png", data.getBuffer());
                                            responseData.crearDaily = data
                                            resemble('./recent/modificarDaily.png')
                                                .compareTo('./older/modificarDaily.png')
                                                .onComplete(function (data) {
                                                    fs.writeFile("./differences/modificarDaily.png", data.getBuffer());
                                                    responseData.modificarDaily = data
                                                    resemble('./recent/eliminarDaily.png')
                                                        .compareTo('./older/eliminarDaily.png')
                                                        .onComplete(function (data) {
                                                            fs.writeFile("./differences/eliminarDaily.png", data.getBuffer());
                                                            responseData.eliminarDaily = data
                                                            resemble('./recent/crearTODO.png')
                                                                .compareTo('./older/crearTODO.png')
                                                                .onComplete(function (data) {
                                                                    fs.writeFile("./differences/crearTODO.png", data.getBuffer());
                                                                    responseData.crearTODO = data
                                                                    resemble('./recent/modificarTODO.png')
                                                                        .compareTo('./older/modificarTODO.png')
                                                                        .onComplete(function (data) {
                                                                            fs.writeFile("./differences/modificarTODO.png", data.getBuffer());
                                                                            responseData.modificarTODO = data
                                                                            resemble('./recent/eliminarTODO.png')
                                                                                .compareTo('./older/eliminarTODO.png')
                                                                                .onComplete(function (data) {
                                                                                    fs.writeFile("./differences/eliminarTODO.png", data.getBuffer());
                                                                                    responseData.eliminarTODO = data
                                                                                    res.send(responseData)
                                                                                    console.log('Finished')
                                                                                });
                                                                        });
                                                                });
                                                        });
                                                });
                                        });
                                });
                        });
                });
        });
});

app.listen(3001, () => {
    console.log('Running regression tests')
})