describe('Habitica', function () {
    it('Visits habitica and logins correctly', function () {
        cy.visit('https://habitica.com/login')
        cy.get('form').find('input[id="usernameInput"]').click().type("KND-Testing")
        cy.get('form').find('input[id="passwordInput"]').click().type("Habbo123")
        cy.get('.btn-info').click()
    })
    it('Closes Bailey', () =>{
        cy.wait(1000)
        cy.get('.modal-footer').find('button[class = "btn btn-warning"]').click()
    })
    it('Closes Start Day', () =>{
        cy.get('.start-day').find('button').click({ force: true })
    })
    it('Creates an habit correctly', function () {
        cy.get('.create-btn').click()
        cy.get('.create-task-btn').find('.icon-habit').click()
        cy.get('.title-input').click().type('This is a test (Habit)')
        cy.get('.modal-footer').find('button').click()
        cy.contains('This is a test (Habit)')
    })
    it('Modifies an habit correctly', function () {
        cy.contains('This is a test (Habit)').click()
        cy.get('.title-input').click().clear().type('This is not a test (Habit)')
        cy.get('.modal-footer').find('button').click()
        cy.contains('This is not a test (Habit)')
    })
    it('Deletes an habit correctly', function () {
        cy.contains('This is not a test (Habit)').click()
        cy.get('.delete-task-btn').click()
    })

    it('Creates a daily correctly', function () {
        cy.get('.create-btn').click()
        cy.get('.create-task-btn').find('.icon-daily').click()
        cy.get('.title-input').click().type('This is a test (daily)')
        cy.get('.modal-footer').find('button').click()
        cy.contains('This is a test (daily)')
    })
    it('Modifies a daily correctly', function () {
        cy.contains('This is a test (daily)').click()
        cy.get('.title-input').click().clear().type('This is not a test (daily)')
        cy.get('.modal-footer').find('button').click()
        cy.contains('This is not a test (daily)')
    })
    it('Deletes a daily correctly', function () {
        cy.contains('This is not a test (daily)').click()
        cy.get('.delete-task-btn').click()
    })

    it('Creates a todo correctly', function () {
        cy.get('.create-btn').click()
        cy.get('.create-task-btn').find('.icon-todo').click()
        cy.get('.title-input').click().type('This is a test (todo)')
        cy.get('.modal-footer').find('button').click()
        cy.contains('This is a test (todo)')
    })
    it('Modifies a todo correctly', function () {
        cy.contains('This is a test (todo)').click()
        cy.get('.title-input').click().clear().type('This is not a test (todo)')
        cy.get('.modal-footer').find('button').click()
        cy.contains('This is not a test (todo)')
    })
    it('Deletes a task correctly', function () {
        cy.contains('This is not a test (todo)').click()
        cy.get('.delete-task-btn').click()
    })
})