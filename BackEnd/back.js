
const express = require('express')
var bodyParser = require('body-parser')
const app = express()
const shell = require('shelljs')
const request = require('request')

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json())

var server = app.listen(80, () => {
    console.log('Running testing machine 1')
})

app.post('/', (req, res) => {
    var data = req.body
    
    if (data.app === "habitica"){
        if (data.test === "e2e"){
            console.log("running habitica e2e tests")
            shell.exec('../HabiticaWeb/e2e/node_modules/.bin/cypress run --project ../HabiticaWeb/e2e/')
            res.send("Your request is being processed you can verify it by sending a request to 35.239.17.191/request/1")
        }
        else if (data.test == "random"){
            console.log("running habitica random tests")
            var options = {
                url: `http://localhost:3000`,
                json: true
            };
            request.get(options, function(error, response, body){
                console.log(response)
                res.send(response)
            })
        }
        else if (data.test == "vrt"){
            console.log("running habitica vrt tests")
            var options = {
                url: `http://localhost:3001`,
                json: true
            };
            request.get(options, function(error, response, body){
                console.log(response)
                res.send(response)
            })
        }
        else{
            res.send('Please specify a valid test')
        }
    }
    else if (data.app == "k9mail"){
    	if (data.test === "mutation"){
    		console.log("running k9mail mutation tests")
            var options = {
                url: `http://localhost:3002`,
                json: true
            };
            request.get(options, function(error, response, body){
                console.log(response)
                res.send(response)
            })
    	}
        if (data.test === "random"){
            console.log("running k9mail random tests")
            var options = {
                url: `http://localhost:3003`,
                json: true
            };
            request.get(options, function(error, response, body){
                console.log(response)
                res.send(response)
            })
        }
        if (data.test === "e2e"){
            console.log("running k9mail E2E tests")
            var options = {
                url: `http://localhost:8000`,
                json: true
            };
            request.get(options, function(error, response, body){
                console.log(response)
                res.send(response)
            })
        }


    }else if (data.app == "loop"){

    }
    else{
        res.send("Error")
    }
})

