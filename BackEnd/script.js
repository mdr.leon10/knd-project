class Queue{
    constructor(){
        this.data = []
    }
}

Queue.prototype.enqueue = function(event){
    this.data.unshift(event)
}
Queue.prototype.dequeue = function(){
    return this.data.pop()
}
Queue.prototype.first = function(){
    return this.data[0]
}
Queue.prototype.last = function(){
    return this.data[this.data.length-1]
}
const express = require('express')
var bodyParser = require('body-parser')
const app = express()
const eventQueue = new Queue()
const request = require("request")

var id = 1
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json())

var server = app.listen(80, () => {
    console.log('Running vm dispatcher')
})

app.post('/', (req, res) => {
    console.log('Llega uno nuevo')
    var data = req.body
    data.id = id
    eventQueue.enqueue(data)
    id = id + 1
    response = []
    var options = {
        url: `http://10.128.0.25`,
        json: true,
        body: data
    };
    for(let i = 0; i < data.amount;i++){
        request.post(options, function(error, response, body){
            console.log(error)
            response.unshift(response)
        })
    }
    res.send("Ok")
})

